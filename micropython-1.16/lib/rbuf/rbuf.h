/*!
* @file    rbuf.h
* @brief   This component implements a ring buffer component based on a array of static memory.
* @version 2.0
* @author  Andrew SU (suyong_yq@126.com)
* @date    2021-10
*/

#ifndef __RBUF_H__
#define __RBUF_H__

#include <stdint.h>
#include <stdbool.h>

/*!
* @brief Define structure for Ring Buffer's rbufx.
*
* The Ring Buffer is origanized as FIFO based on a array of static memory.
*/
typedef struct
{
    uint32_t size;     /*!< Keep the available count of byte in buffer's array */
    uint8_t *mem;      /*!< Keep the first pointer to the buffer's array. This pointer is used to rbufx the memory. */
    uint32_t read_idx; /*!< Keep the index of item in static array for FIFO's read pointer. */
    uint32_t write_idx; /*!< Keep the index of item in static array for FIFO's write pointer. */
    uint32_t count;    /*!< Record the current count of used item in the FIFO      .*/
} rbuf_t;

/*!
* @brief Initialize the ring buffer.
*
* This function fills the allocated static memory into ring buffer's handle structure, and fills the
* read and write pointers with initial value.
*
* @param [out] rbufx Pointer to a empty ring buffer handle to be filled. See to #rbuf_t.
* @param [in]  mem  Pointer to allocated static memory.
* @param [in]  size    Count of memory size in byte.
*/
void rbuf_init(rbuf_t *rbufx, uint8_t *mem, uint32_t size);

/*!
* @brief Reset or Clean the ring buffer.
*
* This function clear all the items in the fifo. Actually, it just resets the write index, read
* index and the item count, then all the old data would be unavailable, while the new data would
* cover them.
*
* @param [in] rbufx Pointer to a ring buffer handle to be clean. See to #rbuf_t.
*/
void rbuf_reset(rbuf_t *rbufx);

/*!
* @brief Check if the buffer is empty.
*
* @param [in] rbufx Pointer to an available rbufx to be operated.
* @retval true  The buffer is empty.
* @retval false The buffer is not empty.
*/
bool rbuf_is_empty(rbuf_t *rbufx);

/*!
* @brief Check if the buffer is full.
*
* @param [in] rbufx Pointer to an available rbufx to be operated.
* @retval true  The buffer is full.
* @retval false The buffer is not full.
*/
bool rbuf_is_full(rbuf_t *rbufx);

/*!
* @brief Put data into buffer.
*
* @param [in] rbufx Pointer to an available rbufx to be operated.
* @param [in] dat The data to be put into buffer.
*/
void rbuf_input(rbuf_t *rbufx, uint8_t dat);

/*!
* @brief Get out data from the buffer.
*
* @param [in] rbufx Pointer to an available rbufx to be operated.
* @retval The data from the buffer.
*/
uint8_t rbuf_output(rbuf_t *rbufx);

uint32_t rbuf_count(rbuf_t *rbufx);

#endif /* __RBUF_H__ */

