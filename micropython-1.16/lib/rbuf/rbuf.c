/* rbuf.c */
#include "rbuf.h"

void rbuf_init(rbuf_t *rbufx, uint8_t *mem, uint32_t size)
{
    rbufx->mem = mem;
    rbufx->size = size;
    rbufx->read_idx = 0u;
    rbufx->write_idx = 0u;
    rbufx->count = 0u;
}

/* clean the fifo. */
void rbuf_reset(rbuf_t *rbufx)
{
    rbufx->write_idx = 0u;
    rbufx->read_idx = 0u;
    rbufx->count = 0u;
}

//static uint32_t _RBUF32_NextIdx(uint32_t idx, uint32_t maxCnt)
//{
//    return ( ((idx+1U) == maxCnt) ? 0U : (idx+1U) );
//}

bool rbuf_is_empty(rbuf_t *rbufx)
{
    //return (rbufx->read_idx == rbufx->write_idx);
    return (rbufx->count == 0u);
}

bool rbuf_is_full(rbuf_t *rbufx)
{
    //return (  _RBUF32_NextIdx(rbufx->write_idx, rbufx->size) == rbufx->read_idx);
    return (rbufx->count == rbufx->size);
}

void rbuf_input(rbuf_t *rbufx, uint8_t dat)
{
    *((rbufx->mem)+(rbufx->write_idx)) = dat; /* 将数据保存在当前指针 */
    //rbufx->write_idx = _RBUF32_NextIdx(rbufx->write_idx, rbufx->size);
    rbufx->write_idx = (rbufx->write_idx+1) % rbufx->size;
    rbufx->count++;
}

uint8_t rbuf_output(rbuf_t *rbufx)
{
    uint8_t dat = *((rbufx->mem)+(rbufx->read_idx)); /* 从当前位置取数 */
    //rbufx->read_idx = _RBUF32_NextIdx(rbufx->read_idx, rbufx->size);
    rbufx->read_idx = (rbufx->read_idx+1) % rbufx->size;
    rbufx->count--;
    return dat;
}

uint32_t rbuf_count(rbuf_t *rbufx)
{
    return rbufx->count;
}

/* EOF. */

