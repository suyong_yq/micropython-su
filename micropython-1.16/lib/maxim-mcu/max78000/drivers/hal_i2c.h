/* hal_i2c.h */
#ifndef __HAL_I2C_H__
#define __HAL_I2C_H__

#include "hal_common.h"

#define I2C_INTFL0_ERRS_MASK (I2C_INTFL0_ARB_ERR_MASK       | \
                              I2C_INTFL0_TO_ERR_MASK        | \
                              I2C_INTFL0_ADDR_NACK_ERR_MASK | \
                              I2C_INTFL0_DATA_ERR_MASK      | \
                              I2C_INTFL0_DNR_ERR_MASK       | \
                              I2C_INTFL0_START_ERR_MASK     | \
                              I2C_INTFL0_STOP_ERR_MASK)

typedef struct
{
    uint32_t ClkSrcHz; /* clock source for apb bus. SystemCoreClcok/2u. */
    uint32_t CommClkHz; /* i2c comm bus clock. */
    uint32_t RxFifoThreshold;
    uint32_t TxFifoThreshold;
} I2C_Master_Init_Type;

bool I2C_InitMaster(I2C_Type *I2Cx, I2C_Master_Init_Type *init);
bool I2C_Recover(I2C_Type *I2Cx, uint32_t retries);
bool I2C_WriteOneReg(I2C_Type *I2Cx, uint8_t dev_addr, uint8_t reg_addr, uint8_t reg_val);
bool I2C_WriteMultiRegs(I2C_Type *I2Cx, uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_vals, uint8_t reg_cnt);
bool I2C_ReadOneReg(I2C_Type *I2Cx, uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_val);

#endif /* __HAL_I2C_H__ */

