/* hal_icc.h */
#ifndef __HAL_ICC_H__
#define __HAL_ICC_H__

#include "hal_common.h"

uint32_t ICC_GetIpVerInfo(ICC_Type * ICCx);
void ICC_EnableCache(ICC_Type * ICCx);
void ICC_DisableCache(ICC_Type * ICCx);

#endif /* __HAL_ICC_H__ */

