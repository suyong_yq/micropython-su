/*******************************************************************************
 * Copyright (C) Maxim Integrated Products, Inc., All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of Maxim Integrated
 * Products, Inc. shall not be used except as stated in the Maxim Integrated
 * Products, Inc. Branding Policy.
 *
 * The mere transfer of this software does not imply any licenses
 * of trade secrets, proprietary technology, copyrights, patents,
 * trademarks, maskwork rights, or any other form of intellectual
 * property whatsoever. Maxim Integrated Products, Inc. retains all
 * ownership rights.
 *
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "max78000.h"
//#include "gcr_regs.h"
//#include "mxc_sys.h"
//#include "icc.h"
#include "hal_icc.h"
#include "hal_clock.h"


//extern void (*const __Vectors[])(void);
//extern void (* const __vector_table[])(void); /* for mdk/armgcc. */
extern void (* const __isr_vector[])(void); /* for iar. */

uint32_t SystemCoreClock = HIRC_FREQ;

const uint32_t sysclks_hz[] =
{
    60000000,  /*  ISO (POR and system reset default). */
    0,         // 1: Reserved
    0,         // 2: Reserved
    8000,      // 3: INRO
    100000000, // 4: IPO
    737280,    // 5: IBRO
    32768,     // 6: ERTCO
    0, // 7: External Clock, EXT_CLK, P0.3, AF1
};

void __attribute__((weak)) SystemCoreClockUpdate(void)
{

    uint32_t sysclk_sel = (GCR->CLKCTRL & GCR_CLKCTRL_SYSCLK_SEL_MASK) >> GCR_CLKCTRL_SYSCLK_SEL_SHIFT;
    uint32_t sysclk_div = (GCR->CLKCTRL & GCR_CLKCTRL_SYSCLK_DIV_MASK) >> GCR_CLKCTRL_SYSCLK_DIV_SHIFT;
    SystemCoreClock  = sysclks_hz[sysclk_sel] >> sysclk_div;
}

/* This function is called just before control is transferred to main().
 *
 * You may over-ride this function in your program by defining a custom
 *  SystemInit(), but care should be taken to reproduce the initialization
 *  steps or a non-functional system may result.
 */
void __attribute__((weak)) SystemInit(void)
{
    SCB->VTOR = (unsigned long)__isr_vector; /* IAR sets the VTOR pointer incorrectly and causes stack corruption */

    /* Make sure interrupts are enabled. */
    //__enable_irq();

    /* Enable instruction cache */
    ICC_EnableCache(ICC0);

    /* Enable FPU on Cortex-M4, which occupies coprocessor slots 10 & 11 */
    /* Grant full access, per "Table B3-24 CPACR bit assignments". */
    /* DDI0403D "ARMv7-M Architecture Reference Manual" */
    SCB->CPACR |= SCB_CPACR_CP10_Msk | SCB_CPACR_CP11_Msk;
    __DSB();
    __ISB();

    /* Change system clock source to the main high-speed clock */
    CLOCK_EnableClkSrc(CLOCK_ClkSrc_IPO);
    CLOCK_SetSysclkSel(CLOCK_ClkSrc_IPO, 1u);

    SystemCoreClockUpdate();
}
