/* mphalport.h */

#ifndef __MPHALPORT_H__
#define __MPHALPORT_H__

#include <stdint.h>
#include "machine_pin.h"


#define MP_HAL_PIN_FMT          "%q"
#define mp_hal_pin_obj_t        const machine_pin_obj_t *
#define mp_hal_get_pin_obj(o)   pin_find(o)
#define mp_hal_pin_name(p)      ((p)->name)

/* for virtual pin: SoftSPI . */
#define mp_hal_pin_write(p, value) (GPIO_WriteBit(p->gpio_port, (1u << p->gpio_pin), value))
#define mp_hal_pin_read(p)  (GPIO_ReadInDataBit(p->gpio_port, (1u << p->gpio_pin)))
#define mp_hal_pin_output(p) machine_pin_set_mode(p, PIN_MODE_OUT_PUSHPULL)
#define mp_hal_pin_input(p)  machine_pin_set_mode(p, PIN_MODE_IN_PULLUP)

/* for soft i2c. */
#define mp_hal_pin_open_drain(p) machine_pin_set_mode(p, PIN_MODE_OUT_OPENDRAIN)
#define mp_hal_pin_high(p)      (GPIO_SetBits(p->gpio_port, (1u << p->gpio_pin)))
#define mp_hal_pin_low(p)       (GPIO_ClearBits(p->gpio_port, (1u << p->gpio_pin)))
#define mp_hal_pin_od_low(p)    mp_hal_pin_low(p)
#define mp_hal_pin_od_high(p)   mp_hal_pin_high(p)

extern volatile uint32_t systick_ms;

static inline mp_uint_t mp_hal_ticks_ms(void)
{
    return systick_ms;
}

static inline mp_uint_t mp_hal_ticks_us(void)
{
    return systick_ms * 1000;
}

static inline mp_uint_t mp_hal_ticks_cpu(void)
{
    return systick_ms;
}

/* return the current ns from very start. */
static inline uint64_t mp_hal_time_ns(void)
{
    return 0u;
}

//#define mp_hal_delay_us_fast(us) mp_hal_delay_us(us)
static inline void mp_hal_delay_us_fast(mp_uint_t us)
{
    for (uint32_t i = us; i > 0u; i--)
    {
        ;
    }
}

static inline void mp_hal_set_interrupt_char(char c) { }
//void mp_hal_set_interrupt_char(int c);

#endif /* __mphalport_h__ */
