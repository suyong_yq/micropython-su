/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Damien P. George
 * Copyright (c) 2020 Jim Mussared
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "py/runtime.h"
#include "py/stream.h"
#include "py/mphal.h"

#include "board_init.h"

int mp_hal_stdin_rx_chr(void)
{
#if 0
    while ( 0u == (UART_STATUS_RX_DONE & UART_GetStatus(BOARD_DEBUG_UART_PORT)) )
    {}
    return UART_GetData(BOARD_DEBUG_UART_PORT);
#endif
    uint8_t ch;
    LPUART_ReadBlocking(BOARD_DEBUG_UART_PORT, &ch, 1u);

    return ch;
}

void mp_hal_stdout_tx_strn(const char *str, mp_uint_t len)
{

#if 0
    while (len--)
    {
        while ( 0u == (UART_STATUS_TX_EMPTY & UART_GetStatus(BOARD_DEBUG_UART_PORT)) )
        {}
        UART_PutData(BOARD_DEBUG_UART_PORT, *str++);
    }
#endif
	LPUART_WriteBlocking(BOARD_DEBUG_UART_PORT, (uint8_t *)str, len);
}


/* Systick的中断服务程序。 */
void SysTick_Handler(void)
{
    systick_ms += 1;
}

/* 原地延时ms。 */
void mp_hal_delay_ms(mp_uint_t ms)
{
    ms += 1;
    uint32_t t0 = systick_ms;
    while (systick_ms - t0 < ms)
    {
        MICROPY_EVENT_POLL_HOOK
    }
}

/* 原地延时us。 */
void mp_hal_delay_us(mp_uint_t us)
{
    uint32_t ms = us / 1000 + 1;
    uint32_t t0 = systick_ms;
    while (systick_ms - t0 < ms)
    {
        __WFI();
    }
}

/* EOF. */

