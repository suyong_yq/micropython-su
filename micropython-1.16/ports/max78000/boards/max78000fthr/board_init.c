#include "board_init.h"

/*
* Definitions.
*/

/*
* Declerations.
*/
/* 系统时基定时器计数值。 */
volatile uint32_t systick_ms = 0u;
extern uint32_t SystemCoreClock;
//void BOARD_InitDebugConsole(void);

/*
* Functions.
*/
void BOARD_Init(void)
{
    BOARD_InitBootClocks();

    BOARD_InitPins();
    BOARD_InitDebugConsole();

    /* Enable Systick. */
    SysTick_Config(SystemCoreClock / 2u / 1000u); /* 1000. */
}

void BOARD_InitDebugConsole(void)
{
    /* setup uart. */
    UART_Init_Type uart_init;
    uart_init.BaudrateClkSrc = UART_BaudrateClkSrc_Alt2; /* ibro. */
    uart_init.BaudrateClkSrcHz = UART_IBRO_FREQ_HZ;
    uart_init.Baudrate = BOARD_DEBUG_UART_BAUDRATE;
    uart_init.DataBits = UART_DataBits_8b;
    uart_init.Parity = UART_Parity_None;
    uart_init.StopBits = UART_StopBits_1b;
    uart_init.RxFifoThreshold = UART_RxFifoThreshold_4B;
    UART_Init(BOARD_DEBUG_UART_PORT, &uart_init);
    UART_ResetTxFifo(BOARD_DEBUG_UART_PORT);
    UART_ResetRxFifo(BOARD_DEBUG_UART_PORT);
    UART_ClearInterruptStatus(BOARD_DEBUG_UART_PORT, UART_GetInterruptStatus(BOARD_DEBUG_UART_PORT));
}

/* EOF. */
