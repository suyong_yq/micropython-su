/* pin_init.c */

#include "pin_init.h"
#include "hal_clock.h"
#include "hal_gpio.h"

void BOARD_InitPins(void)
{
    /* GPIO0.0 - UART0_RX. */
    GPIO_SetPinMode(GPIO0, GPIO_PIN_0, GPIO_PinMode_HighImpedance, false);
    GPIO_SetPinMux(GPIO0, GPIO_PIN_0, GPIO_PinMux_AF_1);

    /* GPIO0.1 - UART0_TX. */
    GPIO_SetPinMode(GPIO0, GPIO_PIN_1, GPIO_PinMode_Out_DriverStrength_3, false);
    GPIO_SetPinMux(GPIO0, GPIO_PIN_1, GPIO_PinMux_AF_1);
}


/* EOF. */
