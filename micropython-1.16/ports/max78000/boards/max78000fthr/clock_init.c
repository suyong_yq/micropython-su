/* clock_init.c */

#include "hal_common.h"
#include "clock_init.h"

#include "hal_clock.h"

void BOARD_InitBootClocks(void)
{
    /* setup clock source. */
    CLOCK_EnableClkSrc(CLOCK_ClkSrc_IPO);
    CLOCK_SetSysclkSel(CLOCK_ClkSrc_IPO, 1u);
    CLOCK_EnableClkSrc(CLOCK_ClkSrc_IBRO); /* for uart. */

    /* release the clock gate from bus to peripherals. */
    CLOCK_EnablePeriph(CLOCK_PERIPH_ID_GPIO0, true);
    CLOCK_EnablePeriph(CLOCK_PERIPH_ID_GPIO1, true);
    CLOCK_EnablePeriph(CLOCK_PERIPH_ID_GPIO2, true);
    CLOCK_EnablePeriph(CLOCK_PERIPH_ID_UART0, true);
}


/* EOF. */
