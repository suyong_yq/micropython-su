/* machine_pin_board_pins. */

#include "hal_common.h"
#include "machine_pin.h"
//#include "machine_dac.h"
//#include "machine_adc.h"
//#include "machine_uart.h"
//#include "machine_spi.h"
//#include "machine_hw_i2c.h"
//#include "machine_pwm.h"
//#include "machine_timer.h"

extern const mp_obj_type_t machine_pin_type;

const uint32_t machine_pin_board_pins_num = 52;

const machine_pin_obj_t pin_P0_0  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_0 , .gpio_port = GPIO0, .gpio_pin = 0  };
const machine_pin_obj_t pin_P0_1  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_1 , .gpio_port = GPIO0, .gpio_pin = 1  };
const machine_pin_obj_t pin_P0_2  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_2 , .gpio_port = GPIO0, .gpio_pin = 2  };
const machine_pin_obj_t pin_P0_3  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_3 , .gpio_port = GPIO0, .gpio_pin = 3  };
const machine_pin_obj_t pin_P0_4  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_4 , .gpio_port = GPIO0, .gpio_pin = 4  };
const machine_pin_obj_t pin_P0_5  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_5 , .gpio_port = GPIO0, .gpio_pin = 5  };
const machine_pin_obj_t pin_P0_6  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_6 , .gpio_port = GPIO0, .gpio_pin = 6  };
const machine_pin_obj_t pin_P0_7  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_7 , .gpio_port = GPIO0, .gpio_pin = 7  };
const machine_pin_obj_t pin_P0_8  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_8 , .gpio_port = GPIO0, .gpio_pin = 8  };
const machine_pin_obj_t pin_P0_9  = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_9 , .gpio_port = GPIO0, .gpio_pin = 9  };
const machine_pin_obj_t pin_P0_10 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_10, .gpio_port = GPIO0, .gpio_pin = 10 };
const machine_pin_obj_t pin_P0_11 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_11, .gpio_port = GPIO0, .gpio_pin = 11 };
const machine_pin_obj_t pin_P0_12 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_12, .gpio_port = GPIO0, .gpio_pin = 12 };
const machine_pin_obj_t pin_P0_13 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_13, .gpio_port = GPIO0, .gpio_pin = 13 };
const machine_pin_obj_t pin_P0_14 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_14, .gpio_port = GPIO0, .gpio_pin = 14 };
const machine_pin_obj_t pin_P0_15 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_15, .gpio_port = GPIO0, .gpio_pin = 15 };
const machine_pin_obj_t pin_P0_16 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_16, .gpio_port = GPIO0, .gpio_pin = 16 };
const machine_pin_obj_t pin_P0_17 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_17, .gpio_port = GPIO0, .gpio_pin = 17 };
const machine_pin_obj_t pin_P0_18 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_18, .gpio_port = GPIO0, .gpio_pin = 18 };
const machine_pin_obj_t pin_P0_19 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_19, .gpio_port = GPIO0, .gpio_pin = 19 };
const machine_pin_obj_t pin_P0_20 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_20, .gpio_port = GPIO0, .gpio_pin = 20 };
const machine_pin_obj_t pin_P0_21 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_21, .gpio_port = GPIO0, .gpio_pin = 21 };
const machine_pin_obj_t pin_P0_22 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_22, .gpio_port = GPIO0, .gpio_pin = 22 };
const machine_pin_obj_t pin_P0_23 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_23, .gpio_port = GPIO0, .gpio_pin = 23 };
const machine_pin_obj_t pin_P0_24 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_24, .gpio_port = GPIO0, .gpio_pin = 24 };
const machine_pin_obj_t pin_P0_25 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_25, .gpio_port = GPIO0, .gpio_pin = 25 };
const machine_pin_obj_t pin_P0_26 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_26, .gpio_port = GPIO0, .gpio_pin = 26 };
const machine_pin_obj_t pin_P0_27 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_27, .gpio_port = GPIO0, .gpio_pin = 27 };
const machine_pin_obj_t pin_P0_28 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_28, .gpio_port = GPIO0, .gpio_pin = 28 };
const machine_pin_obj_t pin_P0_29 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_29, .gpio_port = GPIO0, .gpio_pin = 29 };
const machine_pin_obj_t pin_P0_30 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_30, .gpio_port = GPIO0, .gpio_pin = 30 };
const machine_pin_obj_t pin_P0_31 = { .base = { &machine_pin_type }, .name = MP_QSTR_P0_31, .gpio_port = GPIO0, .gpio_pin = 31 };

const machine_pin_obj_t pin_P1_0  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_0 , .gpio_port = GPIO1, .gpio_pin = 0  };
const machine_pin_obj_t pin_P1_1  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_1 , .gpio_port = GPIO1, .gpio_pin = 1  };
const machine_pin_obj_t pin_P1_2  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_2 , .gpio_port = GPIO1, .gpio_pin = 2  };
const machine_pin_obj_t pin_P1_3  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_3 , .gpio_port = GPIO1, .gpio_pin = 3  };
const machine_pin_obj_t pin_P1_4  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_4 , .gpio_port = GPIO1, .gpio_pin = 4  };
const machine_pin_obj_t pin_P1_5  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_5 , .gpio_port = GPIO1, .gpio_pin = 5  };
const machine_pin_obj_t pin_P1_6  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_6 , .gpio_port = GPIO1, .gpio_pin = 6  };
const machine_pin_obj_t pin_P1_7  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_7 , .gpio_port = GPIO1, .gpio_pin = 7  };
const machine_pin_obj_t pin_P1_8  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_8 , .gpio_port = GPIO1, .gpio_pin = 8  };
const machine_pin_obj_t pin_P1_9  = { .base = { &machine_pin_type }, .name = MP_QSTR_P1_9 , .gpio_port = GPIO1, .gpio_pin = 9  };

const machine_pin_obj_t pin_P2_0  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_0 , .gpio_port = GPIO2, .gpio_pin = 0  };
const machine_pin_obj_t pin_P2_1  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_1 , .gpio_port = GPIO2, .gpio_pin = 1  };
const machine_pin_obj_t pin_P2_2  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_2 , .gpio_port = GPIO2, .gpio_pin = 2  };
const machine_pin_obj_t pin_P2_3  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_3 , .gpio_port = GPIO2, .gpio_pin = 3  };
const machine_pin_obj_t pin_P2_4  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_4 , .gpio_port = GPIO2, .gpio_pin = 4  };
const machine_pin_obj_t pin_P2_5  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_5 , .gpio_port = GPIO2, .gpio_pin = 5  };
const machine_pin_obj_t pin_P2_6  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_6 , .gpio_port = GPIO2, .gpio_pin = 6  };
const machine_pin_obj_t pin_P2_7  = { .base = { &machine_pin_type }, .name = MP_QSTR_P2_7 , .gpio_port = GPIO2, .gpio_pin = 7  };

const machine_pin_obj_t pin_P3_0  = { .base = { &machine_pin_type }, .name = MP_QSTR_P3_0 , .gpio_port = GPIO3, .gpio_pin = 1  };
const machine_pin_obj_t pin_P3_1  = { .base = { &machine_pin_type }, .name = MP_QSTR_P3_1 , .gpio_port = GPIO3, .gpio_pin = 2  };

/* pin id in the package. */
const machine_pin_obj_t* machine_pin_board_pins[] =
{
    &pin_P0_0 ,
    &pin_P0_1 ,
    &pin_P0_2 ,
    &pin_P0_3 ,
    &pin_P0_4 ,
    &pin_P0_5 ,
    &pin_P0_6 ,
    &pin_P0_7 ,
    &pin_P0_8 ,
    &pin_P0_9 ,
    &pin_P0_10,
    &pin_P0_11,
    &pin_P0_12,
    &pin_P0_13,
    &pin_P0_14,
    &pin_P0_15,
    &pin_P0_16,
    &pin_P0_17,
    &pin_P0_18,
    &pin_P0_19,
    &pin_P0_20,
    &pin_P0_21,
    &pin_P0_22,
    &pin_P0_23,
    &pin_P0_24,
    &pin_P0_25,
    &pin_P0_26,
    &pin_P0_27,
    &pin_P0_28,
    &pin_P0_29,
    &pin_P0_30,
    &pin_P0_31,
    &pin_P1_0 ,
    &pin_P1_1 ,
    &pin_P1_2 ,
    &pin_P1_3 ,
    &pin_P1_4 ,
    &pin_P1_5 ,
    &pin_P1_6 ,
    &pin_P1_7 ,
    &pin_P1_8 ,
    &pin_P1_9 ,
    &pin_P2_0 ,
    &pin_P2_1 ,
    &pin_P2_2 ,
    &pin_P2_3 ,
    &pin_P2_4 ,
    &pin_P2_5 ,
    &pin_P2_6 ,
    &pin_P2_7 ,
    &pin_P3_0 ,
    &pin_P3_1 ,
};

STATIC const mp_rom_map_elem_t machine_pin_board_pins_locals_dict_table[] =
{

    { MP_ROM_QSTR(MP_QSTR_P0_0 ), MP_ROM_PTR( &pin_P0_0 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_1 ), MP_ROM_PTR( &pin_P0_1 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_2 ), MP_ROM_PTR( &pin_P0_2 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_3 ), MP_ROM_PTR( &pin_P0_3 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_4 ), MP_ROM_PTR( &pin_P0_4 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_5 ), MP_ROM_PTR( &pin_P0_5 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_6 ), MP_ROM_PTR( &pin_P0_6 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_7 ), MP_ROM_PTR( &pin_P0_7 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_8 ), MP_ROM_PTR( &pin_P0_8 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_9 ), MP_ROM_PTR( &pin_P0_9 ) },
    { MP_ROM_QSTR(MP_QSTR_P0_10), MP_ROM_PTR( &pin_P0_10) },
    { MP_ROM_QSTR(MP_QSTR_P0_11), MP_ROM_PTR( &pin_P0_11) },
    { MP_ROM_QSTR(MP_QSTR_P0_12), MP_ROM_PTR( &pin_P0_12) },
    { MP_ROM_QSTR(MP_QSTR_P0_13), MP_ROM_PTR( &pin_P0_13) },
    { MP_ROM_QSTR(MP_QSTR_P0_14), MP_ROM_PTR( &pin_P0_14) },
    { MP_ROM_QSTR(MP_QSTR_P0_15), MP_ROM_PTR( &pin_P0_15) },
    { MP_ROM_QSTR(MP_QSTR_P0_16), MP_ROM_PTR( &pin_P0_16) },
    { MP_ROM_QSTR(MP_QSTR_P0_17), MP_ROM_PTR( &pin_P0_17) },
    { MP_ROM_QSTR(MP_QSTR_P0_18), MP_ROM_PTR( &pin_P0_18) },
    { MP_ROM_QSTR(MP_QSTR_P0_19), MP_ROM_PTR( &pin_P0_19) },
    { MP_ROM_QSTR(MP_QSTR_P0_20), MP_ROM_PTR( &pin_P0_20) },
    { MP_ROM_QSTR(MP_QSTR_P0_21), MP_ROM_PTR( &pin_P0_21) },
    { MP_ROM_QSTR(MP_QSTR_P0_22), MP_ROM_PTR( &pin_P0_22) },
    { MP_ROM_QSTR(MP_QSTR_P0_23), MP_ROM_PTR( &pin_P0_23) },
    { MP_ROM_QSTR(MP_QSTR_P0_24), MP_ROM_PTR( &pin_P0_24) },
    { MP_ROM_QSTR(MP_QSTR_P0_25), MP_ROM_PTR( &pin_P0_25) },
    { MP_ROM_QSTR(MP_QSTR_P0_26), MP_ROM_PTR( &pin_P0_26) },
    { MP_ROM_QSTR(MP_QSTR_P0_27), MP_ROM_PTR( &pin_P0_27) },
    { MP_ROM_QSTR(MP_QSTR_P0_28), MP_ROM_PTR( &pin_P0_28) },
    { MP_ROM_QSTR(MP_QSTR_P0_29), MP_ROM_PTR( &pin_P0_29) },
    { MP_ROM_QSTR(MP_QSTR_P0_30), MP_ROM_PTR( &pin_P0_30) },
    { MP_ROM_QSTR(MP_QSTR_P0_31), MP_ROM_PTR( &pin_P0_31) },
    { MP_ROM_QSTR(MP_QSTR_P1_0 ), MP_ROM_PTR( &pin_P1_0 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_1 ), MP_ROM_PTR( &pin_P1_1 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_2 ), MP_ROM_PTR( &pin_P1_2 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_3 ), MP_ROM_PTR( &pin_P1_3 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_4 ), MP_ROM_PTR( &pin_P1_4 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_5 ), MP_ROM_PTR( &pin_P1_5 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_6 ), MP_ROM_PTR( &pin_P1_6 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_7 ), MP_ROM_PTR( &pin_P1_7 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_8 ), MP_ROM_PTR( &pin_P1_8 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P1_9 ), MP_ROM_PTR( &pin_P1_9 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_0 ), MP_ROM_PTR( &pin_P2_0 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_1 ), MP_ROM_PTR( &pin_P2_1 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_2 ), MP_ROM_PTR( &pin_P2_2 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_3 ), MP_ROM_PTR( &pin_P2_3 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_4 ), MP_ROM_PTR( &pin_P2_4 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_5 ), MP_ROM_PTR( &pin_P2_5 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_6 ), MP_ROM_PTR( &pin_P2_6 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P2_7 ), MP_ROM_PTR( &pin_P2_7 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P3_0 ), MP_ROM_PTR( &pin_P3_0 ) }, 
    { MP_ROM_QSTR(MP_QSTR_P3_1 ), MP_ROM_PTR( &pin_P3_1 ) }, 

};

MP_DEFINE_CONST_DICT(machine_pin_board_pins_locals_dict, machine_pin_board_pins_locals_dict_table);


#if 0

/* UART. */
const uint32_t machine_uart_num = 3u;

machine_uart_conf_t machine_uart_conf[3]; /* static mamory instead of malloc(). */
LPUART_Type * const machine_uart_port[3] = {LPUART0, LPUART1, LPUART2};

const machine_uart_obj_t uart_0 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PB0, .rx_pin_af = 2, .tx_pin_obj = &pin_PB1, .tx_pin_af = 2, .uart_port = machine_uart_port[0], .uart_irqn = LPUART0_RX_IRQn, .uart_id = 0u, .conf = &machine_uart_conf[0]};
const machine_uart_obj_t uart_1 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PC8, .rx_pin_af = 2, .tx_pin_obj = &pin_PC9, .tx_pin_af = 2, .uart_port = machine_uart_port[1], .uart_irqn = LPUART1_RX_IRQn, .uart_id = 1u, .conf = &machine_uart_conf[1]};
const machine_uart_obj_t uart_2 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PD6, .rx_pin_af = 2, .tx_pin_obj = &pin_PD7, .tx_pin_af = 2, .uart_port = machine_uart_port[2], .uart_irqn = LPUART2_RX_IRQn, .uart_id = 2u, .conf = &machine_uart_conf[2]};

const machine_uart_obj_t * machine_uart_objs[] =
{
    &uart_0,
    &uart_1,
    &uart_2,
};

/* I2C. */
const uint32_t machine_hw_i2c_num = 1u;

machine_hw_i2c_conf_t machine_hw_i2c_conf[1]; /* static mamory instead of malloc(). */
LPI2C_Type * const machine_hw_i2c_port[1] = {LPI2C0};

const machine_hw_i2c_obj_t hw_i2c_0 = { .base = { &machine_hw_i2c_type }, .sda_pin_obj = &pin_PA2, .sda_pin_af = 3, .scl_pin_obj = &pin_PA3, .scl_pin_af = 3, .i2c_port = machine_hw_i2c_port[0], .i2c_id = 0u, .conf = &machine_hw_i2c_conf[0]};

const machine_hw_i2c_obj_t * machine_hw_i2c_objs[] =
{
    &hw_i2c_0,
};

#endif

/* EOF. */

