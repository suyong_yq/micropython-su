/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020-2021 Damien P. George
 * Copyright (c) 2021 Philipp Ebensberger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>

#include "py/runtime.h"
#include "extmod/vfs.h"
#include "modmm32f3.h"
#include "sfud.h"

// BOARD_FLASH_SIZE is defined in mpconfigport.h

#define SECTOR_SIZE_BYTES (4096)
#define PAGE_SIZE_BYTES   (256)

typedef struct _mm32f3_flash_obj_t
{
    mp_obj_base_t base;
    uint32_t flash_base;
    uint32_t flash_size;
} mm32f3_flash_obj_t;

/* 返回唯一的flash实例化对象 */
STATIC mm32f3_flash_obj_t mm32f3_flash_obj =
{
    .base = { &mm32f3_flash_type }
};

/* 擦除包含addr在内的整个扇区。 */
// flash_erase_block(erase_addr_bytes)
// erases the 4k sector starting at adddr
//status_t flash_erase_block(uint32_t erase_addr) __attribute__((section(".ram_functions")));
uint32_t flash_erase_block(uint32_t erase_addr)
{
    sfud_erase(sfud_get_device(0), erase_addr, SECTOR_SIZE_BYTES);

    return 0u;
}

// flash_write_block(flash_dest_addr_bytes, data_source, length_bytes)
// writes length_byte data to the destination address
// length is a multiple of the page size = 256
// the vfs driver takes care for erasing the sector if required
//status_t flash_write_block(uint32_t dest_addr, const uint8_t *src, uint32_t length) __attribute__((section(".ram_functions")));
uint32_t flash_write_block(uint32_t dest_addr, const uint8_t *src, uint32_t length)
{
    // write sector in page size chunks
    for (int i = 0; i < length; i += PAGE_SIZE_BYTES) {
        sfud_write(sfud_get_device(0), dest_addr + i, PAGE_SIZE_BYTES, (uint8_t *)(src + i));
    }

    return 0u;
}

STATIC mp_obj_t mm32f3_flash_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *all_args)
{
    // Check args.
    mp_arg_check_num(n_args, n_kw, 0, 0, false);

    /* setup the sfud and spiflash hardware. */
    sfud_init(); /* init sfud. */
    sfud_flash * flash = sfud_get_device(0);
    //sfud_qspi_fast_read_enable(flash, 4); /* enable qspi_read. */

    mm32f3_flash_obj.flash_base = 0u; //MICROPY_HW_FLASH_STORAGE_BASE;
    mm32f3_flash_obj.flash_size = flash->chip.capacity ;//MICROPY_HW_FLASH_STORAGE_BYTES;

    // Return singleton object.
    return MP_OBJ_FROM_PTR(&mm32f3_flash_obj);
}

// readblocks(block_num, buf, [offset])
// read size of buffer number of bytes from block (with offset) into buffer
STATIC mp_obj_t mm32f3_flash_readblocks(size_t n_args, const mp_obj_t *args)
{
    mm32f3_flash_obj_t * self = MP_OBJ_TO_PTR(args[0]);
    mp_buffer_info_t bufinfo;
    mp_get_buffer_raise(args[2], &bufinfo, MP_BUFFER_WRITE);

    // Calculate read offset from block number.
    uint32_t offset = mp_obj_get_int(args[1]) * SECTOR_SIZE_BYTES;
    // Add optional offset
    if (n_args == 4) {
        offset += mp_obj_get_int(args[3]);
    }

    sfud_read(sfud_get_device(0), self->flash_base + offset, bufinfo.len, bufinfo.buf);

    return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(mm32f3_flash_readblocks_obj, 3, 4, mm32f3_flash_readblocks);

// writeblocks(block_num, buf, [offset])
// Erase block based on block_num and write buffer size number of bytes from buffer into block. If additional offset
// parameter is provided only write operation at block start + offset will be performed.
// This requires a prior erase operation of the block!
STATIC mp_obj_t mm32f3_flash_writeblocks(size_t n_args, const mp_obj_t *args)
{
    //status_t status;
    mm32f3_flash_obj_t * self = MP_OBJ_TO_PTR(args[0]);
    mp_buffer_info_t bufinfo;
    mp_get_buffer_raise(args[2], &bufinfo, MP_BUFFER_READ);


    // Calculate read offset from block number.
    uint32_t offset = mp_obj_get_int(args[1]) * SECTOR_SIZE_BYTES;

    if (n_args == 3)
    {
        flash_erase_block(self->flash_base + offset);
    } else
    {
        // Add optional offset
        offset += mp_obj_get_int(args[3]);
    }

    flash_write_block(self->flash_base + offset, bufinfo.buf, bufinfo.len);

    return MP_OBJ_NEW_SMALL_INT(0);
}
STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(mm32f3_flash_writeblocks_obj, 3, 4, mm32f3_flash_writeblocks);

// ioctl(op, arg)
STATIC mp_obj_t mm32f3_flash_ioctl(mp_obj_t self_in, mp_obj_t cmd_in, mp_obj_t arg_in) {
    mm32f3_flash_obj_t *self = MP_OBJ_TO_PTR(self_in);
    mp_int_t cmd = mp_obj_get_int(cmd_in);
    //status_t status;
    switch (cmd) {
        case MP_BLOCKDEV_IOCTL_INIT:
            return MP_OBJ_NEW_SMALL_INT(0);
        case MP_BLOCKDEV_IOCTL_DEINIT:
            return MP_OBJ_NEW_SMALL_INT(0);
        case MP_BLOCKDEV_IOCTL_SYNC:
            return MP_OBJ_NEW_SMALL_INT(0);
        case MP_BLOCKDEV_IOCTL_BLOCK_COUNT:
            return MP_OBJ_NEW_SMALL_INT(self->flash_size / SECTOR_SIZE_BYTES);
        case MP_BLOCKDEV_IOCTL_BLOCK_SIZE:
            return MP_OBJ_NEW_SMALL_INT(SECTOR_SIZE_BYTES);
        case MP_BLOCKDEV_IOCTL_BLOCK_ERASE: {
            uint32_t offset = mp_obj_get_int(arg_in) * SECTOR_SIZE_BYTES;
            flash_erase_block(self->flash_base + offset);
            return MP_OBJ_NEW_SMALL_INT(0);
        }
        default:
            return mp_const_none;
    }
}
STATIC MP_DEFINE_CONST_FUN_OBJ_3(mm32f3_flash_ioctl_obj, mm32f3_flash_ioctl);

STATIC const mp_rom_map_elem_t mm32f3_flash_locals_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_readblocks), MP_ROM_PTR(&mm32f3_flash_readblocks_obj) },
    { MP_ROM_QSTR(MP_QSTR_writeblocks), MP_ROM_PTR(&mm32f3_flash_writeblocks_obj) },
    { MP_ROM_QSTR(MP_QSTR_ioctl), MP_ROM_PTR(&mm32f3_flash_ioctl_obj) },
};
STATIC MP_DEFINE_CONST_DICT(mm32f3_flash_locals_dict, mm32f3_flash_locals_dict_table);

const mp_obj_type_t mm32f3_flash_type = {
    { &mp_type_type },
    .name = MP_QSTR_Flash,
    .make_new = mm32f3_flash_make_new,
    .locals_dict = (mp_obj_dict_t *)&mm32f3_flash_locals_dict,
};
