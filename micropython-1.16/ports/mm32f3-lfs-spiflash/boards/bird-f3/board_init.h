/* board_init.h */
#ifndef __BOARD_INIT_H__
#define __BOARD_INIT_H__

#include <stdio.h>
#include <stdint.h>

#include "hal_common.h"
#include "hal_rcc.h"
#include "hal_uart.h"
#include "hal_gpio.h"
#include "hal_tim_16b.h"
#include "hal_tim_adv.h"
#include "hal_spi.h"

#include "clock_init.h"
#include "pin_init.h"

/* DEBUG UART. */
#define BOARD_DEBUG_UART_PORT        UART1
#define BOARD_DEBUG_UART_BAUDRATE    115200u
#define BOARD_DEBUG_UART_FREQ        CLOCK_APB2_FREQ

#define BOARD_SDCARD_SDIO_PORT       SDIO

#define BOARD_TIM_BASIC_FREQ         CLOCK_APB1_FREQ
#define BOARD_TIM_16B_FREQ           CLOCK_APB2_FREQ

/* SPI3. */
#define BOARD_FLASH_SPI_PORT          SPI2
#define BOARD_FLASH_SPI_BAUDRATE      400000u /* 400khz. */
#define BOARD_FLASH_SPI_FREQ          CLOCK_APB1_FREQ

/* PF6. spiflash cs. */
#define BOARD_FLASH_CS_GPIO_PORT      GPIOF
#define BOARD_FLASH_CS_GPIO_PIN       GPIO_PIN_6

#define BOARD_FLASH_RX_GPIO_PORT      GPIOF
#define BOARD_FLASH_RX_GPIO_PIN       GPIO_PIN_8

#define BOARD_FLASH_TX_GPIO_PORT      GPIOG
#define BOARD_FLASH_TX_GPIO_PIN       GPIO_PIN_6

#define BOARD_FLASH_CLK_GPIO_PORT     GPIOG
#define BOARD_FLASH_CLK_GPIO_PIN      GPIO_PIN_7





void BOARD_Init(void);

void BOARD_InitDebugConsole(void);

extern volatile uint32_t systick_ms;

#endif /* __BOARD_INIT_H__ */

