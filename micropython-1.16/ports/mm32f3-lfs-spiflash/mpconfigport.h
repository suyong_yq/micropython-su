/* mpconfigport.h */
// Options controlling how MicroPython is built, overriding defaults in py/mpconfig.h

// Board specific definitions
#include "mpconfigboard.h"

// Memory allocation policies
#define MICROPY_GC_STACK_ENTRY_TYPE         uint16_t
#define MICROPY_GC_ALLOC_THRESHOLD          (0)
#define MICROPY_ALLOC_PARSE_CHUNK_INIT      (32)
#define MICROPY_ALLOC_PATH_MAX              (256)
#define MICROPY_QSTR_BYTES_IN_HASH          (1)

// Compiler configuration
#define MICROPY_COMP_CONST                  (1)

// Python internal features
#define MICROPY_ENABLE_GC                   (1)
#define MICROPY_KBD_EXCEPTION               (1)
#define MICROPY_HELPER_REPL                 (1)
#define MICROPY_LONGINT_IMPL                (MICROPY_LONGINT_IMPL_MPZ)
#define MICROPY_ENABLE_SOURCE_LINE          (1)
#define MICROPY_ERROR_REPORTING             (MICROPY_ERROR_REPORTING_TERSE)
#define MICROPY_CPYTHON_COMPAT              (0)
#define MICROPY_CAN_OVERRIDE_BUILTINS       (1)

// Control over Python builtins
#define MICROPY_PY_ASYNC_AWAIT              (0)
#define MICROPY_PY_BUILTINS_STR_COUNT       (0)
#define MICROPY_PY_BUILTINS_MEMORYVIEW      (1)
#define MICROPY_PY_BUILTINS_SET             (0)
#define MICROPY_PY_BUILTINS_FROZENSET       (0)
#define MICROPY_PY_BUILTINS_PROPERTY        (0)
#define MICROPY_PY_BUILTINS_ENUMERATE       (0)
#define MICROPY_PY_BUILTINS_FILTER          (0)
#define MICROPY_PY_BUILTINS_REVERSED        (0)
#define MICROPY_PY_BUILTINS_MIN_MAX         (0)
#define MICROPY_PY___FILE__                 (0)
#define MICROPY_PY_MICROPYTHON_MEM_INFO     (1)
#define MICROPY_PY_ARRAY_SLICE_ASSIGN       (1)
#define MICROPY_PY_ATTRTUPLE                (0)
#define MICROPY_PY_COLLECTIONS              (0)
#define MICROPY_PY_SYS_MAXSIZE              (1)

#define MICROPY_MODULE_FROZEN          (1) /* enable pyexec_frozen_module() in pyexec.c */
#define MICROPY_MODULE_FROZEN_MPY      (1)

// Use VfsLfs2's types for fileio/textio
#define mp_type_fileio mp_type_vfs_lfs2_fileio
#define mp_type_textio mp_type_vfs_lfs2_textio

// fatfs configuration used in ffconf.h
#define MICROPY_VFS                    (1)
//#define MICROPY_VFS_FAT                (1)
#define MICROPY_FATFS                  (1)
#define MICROPY_FATFS_ENABLE_LFN       (1)
#define MICROPY_FATFS_LFN_CODE_PAGE    437 /* 1=SFN/ANSI 437=LFN/U.S.(OEM) */
#define MICROPY_FATFS_USE_LABEL        (1)
#define MICROPY_FATFS_RPATH            (2)
#define MICROPY_FATFS_MULTI_PARTITION  (1)

#define MICROPY_READER_VFS             (1) /* enable mp_lexer_new_from_file() in lexer.c */
#define MICROPY_ENABLE_COMPILER        (1) /* enable lexer.c */

/* Enable floating point number and math module. */
#define MICROPY_PY_BUILTINS_FLOAT           (1) /* enable floating point number or not. */
#define MICROPY_PY_BUILTINS_COMPLEX         (1) /* enable complex number or not. */
#define MICROPY_FLOAT_IMPL                  (MICROPY_FLOAT_IMPL_FLOAT) /* floating with single or double. */
#define MICROPY_PY_MATH                     (1) /* enable math or not. */
#define MICROPY_PY_CMATH                    (1) /* enable cmath or not. */

#define MICROPY_PY_BUILTINS_HELP            (1) /* enable help(). */
#define MICROPY_HELPER_REPL                 (1)

#define MICROPY_ENABLE_SCHEDULER       (1) /* enable scheduler components, using in irq. */

// Extended modules
#define MICROPY_PY_UTIME_MP_HAL        (1)
#define MICROPY_PY_MACHINE             (1)
#define MICROPY_PY_MACHINE_SPI         (1) /* enable extmod/machine_spi.c */
#define MICROPY_PY_MACHINE_I2C         (1) /* enable extmod/machine_i2c.c */

// use vfs's functions for import stat and builtin open
#define mp_import_stat              mp_vfs_import_stat
#define mp_builtin_open             mp_vfs_open
#define mp_builtin_open_obj         mp_vfs_open_obj

// Hooks to add builtins
#define MICROPY_PORT_BUILTINS \
    { MP_ROM_QSTR(MP_QSTR_open), MP_ROM_PTR(&mp_builtin_open_obj) },

extern const struct _mp_obj_module_t mp_module_machine;
extern const struct _mp_obj_module_t mp_module_utime;
extern const struct _mp_obj_module_t mp_module_uos;
extern const struct _mp_obj_module_t mp_module_mm32f3;

#define MICROPY_PORT_BUILTIN_MODULES \
    { MP_ROM_QSTR(MP_QSTR_machine), MP_ROM_PTR(&mp_module_machine) }, \
    { MP_ROM_QSTR(MP_QSTR_time), MP_ROM_PTR(&mp_module_utime) }, \
    { MP_ROM_QSTR(MP_QSTR_os), MP_ROM_PTR(&mp_module_uos) }, \
    { MP_ROM_QSTR(MP_QSTR_mm32f3), MP_ROM_PTR(&mp_module_mm32f3) }, \


#define MICROPY_PORT_ROOT_POINTERS \
    const char *readline_hist[8];

#define MP_STATE_PORT MP_STATE_VM

// Miscellaneous settings

#define MICROPY_EVENT_POLL_HOOK \
    do { \
        extern void mp_handle_pending(bool); \
        mp_handle_pending(true); \
        __WFI(); \
    } while (0);

#define MICROPY_MAKE_POINTER_CALLABLE(p) ((void *)((mp_uint_t)(p) | 1))

#define MP_SSIZE_MAX (0x7fffffff)
typedef int mp_int_t; // must be pointer size
typedef unsigned mp_uint_t; // must be pointer size
typedef long mp_off_t;

// Need to provide a declaration/definition of alloca()
#include <alloca.h>

/* EOF. */

