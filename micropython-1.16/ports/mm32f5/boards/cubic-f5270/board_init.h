/* board_init.h */
#ifndef __BOARD_INIT_H__
#define __BOARD_INIT_H__

#include <stdio.h>
#include <stdint.h>

#include "hal_common.h"
#include "hal_rcc.h"
#include "hal_uart.h"
#include "hal_gpio.h"

#include "clock_init.h"
#include "pin_init.h"

/* DEBUG UART. */
#define BOARD_DEBUG_UART_PORT        UART1
#define BOARD_DEBUG_UART_BAUDRATE    115200u
#define BOARD_DEBUG_UART_FREQ        CLOCK_APB2_FREQ


void BOARD_Init(void);

void BOARD_InitDebugConsole(void);

extern volatile uint32_t systick_ms;

#endif /* __BOARD_INIT_H__ */

