/* machine_uart.h */

#ifndef __MACHINE_UART_H__
#define __MACHINE_UART_H__

#include "py/runtime.h"
#include "py/stream.h"

#include "machine_pin.h"
#include "hal_uart.h"
#include "rbuf.h"

#define MACHINE_UART_XFER_BUFF_LEN  64u
#define MACHINE_UART_NUM            8u /* machine_uart_num. */

typedef enum
{
    UART_PARITY_NONE = 0u,
    UART_PARITY_EVEN,
    UART_PARITY_ODD,
} machine_uart_parity_t;

/* UART class instance configuration structure. */
typedef struct
{
    uint32_t baudrate;
    //uint32_t bits; /* 8, 9 */
    //uint32_t parity; /* 0 for even, 1 for odd. */
    //uint32_t stop; /* 1, 2 */

    rbuf_t * rx_rbuf;
    //rbuf_t * tx_rbuf;

} machine_uart_conf_t;

typedef struct
{
    mp_obj_base_t base;      // object base class.

    const machine_pin_obj_t * rx_pin_obj;
    uint32_t rx_pin_af;

    const machine_pin_obj_t * tx_pin_obj;
    uint32_t tx_pin_af;

    UART_Type * uart_port;
    IRQn_Type   uart_irqn;
    uint32_t    uart_id;

    machine_uart_conf_t * conf;
} machine_uart_obj_t;


extern const machine_uart_obj_t * machine_uart_objs[];
extern const uint32_t             machine_uart_num;
extern       UART_Type    * const machine_uart_port[];
extern const mp_obj_type_t        machine_uart_type;

const machine_uart_obj_t *uart_find(mp_obj_t user_obj);


void machine_uart_hw_init(const machine_uart_obj_t *self, UART_Init_Type * init);
bool machine_uart_tx_data(uint32_t uart_id, uint8_t tx_data);
uint8_t machine_uart_rx_data(uint32_t uart_id);




#endif /* __MACHINE_UART_H__ */

