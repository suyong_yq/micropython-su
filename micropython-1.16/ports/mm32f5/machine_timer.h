/* machine_timer.h */

#ifndef __MACHINE_TIMER_H__
#define __MACHINE_TIMER_H__

#include "py/runtime.h"
#include "py/obj.h"

#include "hal_tim.h"


/* TIMER class instance configuration structure. */

#define MACHINE_TIMER_NUM   2u
#define TIMER_MODE_ONE_SHOT (0)
#define TIMER_MODE_PERIODIC (1)

typedef struct
{
    mp_obj_t callback; /* Python传来的回调函数 */
    uint32_t period;   /* 超时时长，以ms为单位 */
    //uint32_t tick_hz;  /*  */
    //uint32_t freq;     /*  */
    uint32_t mode;  /* 工作模式： ONE_SHOT | PERIODIC */
} machine_timer_conf_t;

typedef struct
{
    mp_obj_base_t    base;      // object base class.
    TIM_Type       * timer_port;
    IRQn_Type        timer_irqn;
    uint32_t         timer_id;
    machine_timer_conf_t *conf;
} machine_timer_obj_t;

extern const machine_timer_obj_t * machine_timer_objs[];
extern const mp_obj_type_t         machine_timer_type;

const machine_timer_obj_t *timer_find(mp_obj_t user_obj);

#endif /* __MACHINE_TIMER_H__ */

