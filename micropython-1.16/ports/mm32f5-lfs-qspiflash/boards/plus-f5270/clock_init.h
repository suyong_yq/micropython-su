/* clock_init.h */
#ifndef __CLOCK_INIT_H__
#define __CLOCK_INIT_H__

#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"

#if 0
#define CLOCK_SYS_FREQ         96000000u
#define CLOCK_SYSTICK_FREQ     (CLOCK_SYS_FREQ / 8u)
#define CLOCK_AHB1_FREQ        96000000u
#define CLOCK_AHB2_FREQ        96000000u
#define CLOCK_AHB3_FREQ        96000000u
#define CLOCK_APB1_FREQ        48000000u
#define CLOCK_APB2_FREQ        48000000u
#endif

#if 0
#define CLOCK_SYS_FREQ         48000000u
#define CLOCK_SYSTICK_FREQ     (CLOCK_SYS_FREQ/8u)
#define CLOCK_AHB1_FREQ        48000000u
#define CLOCK_AHB2_FREQ        48000000u
#define CLOCK_AHB3_FREQ        48000000u
#define CLOCK_APB1_FREQ        24000000u
#define CLOCK_APB2_FREQ        24000000u
#endif

#if 1
#define CLOCK_SYS_FREQ         120000000u
#define CLOCK_SYSTICK_FREQ     (CLOCK_SYS_FREQ / 8u)
#define CLOCK_AHB1_FREQ        120000000u
#define CLOCK_AHB2_FREQ        120000000u
#define CLOCK_AHB3_FREQ        120000000u
#define CLOCK_APB1_FREQ        60000000u
#define CLOCK_APB2_FREQ        60000000u
#endif

void BOARD_InitBootClocks(void);

#endif /* __CLOCK_INIT_H__ */

