/* board_init.c */
#include "board_init.h"

/*
* Definitions.
*/

/*
* Declerations.
*/

volatile uint32_t systick_ms = 0u;



/*
* Functions.
*/
void BOARD_Init(void)
{
    BOARD_InitBootClocks();
    BOARD_InitPins();

    //BOARD_InitDebugConsole();

    //TIM_ADV_EnableOutputCompareSwitch(TIM1, true);
    //TIM_ADV_EnableOutputCompareSwitch(TIM8, true);

    /* Enable Systick. */
    SysTick_Config(CLOCK_SYSTICK_FREQ / 100u); /* 1000. */
}

#if 1
void BOARD_InitDebugConsole(void)
{
    /* UART1. */
    UART_Init_Type uart_init;

    uart_init.ClockFreqHz   = BOARD_DEBUG_UART_FREQ; /* 48mhz, APB2. */
    uart_init.BaudRate      = BOARD_DEBUG_UART_BAUDRATE;
    uart_init.WordLength    = UART_WordLength_8b;
    uart_init.StopBits      = UART_StopBits_1;
    uart_init.Parity        = UART_Parity_None;
    uart_init.XferMode      = UART_XferMode_RxTx;
    uart_init.HwFlowControl = UART_HwFlowControl_None;
    UART_Init(BOARD_DEBUG_UART_PORT, &uart_init);
    UART_Enable(BOARD_DEBUG_UART_PORT, true);
}
#endif

#if 0
BOARD_BootAppSelMode_T BOARD_GetBootAppSelMode(void)
{
    BOARD_BootAppSelMode_T boot_mode = BOARD_BootAppSelMode_Normal; /* default. */

    /* button. */
    GPIO_Init_Type gpio_init;
    gpio_init.Pins  = BOARD_BOOT_APP_SEL_MODE_GPIO_0_PIN;
    gpio_init.PinMode  = GPIO_PinMode_In_PullUp; /* input. */
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(BOARD_BOOT_APP_SEL_MODE_GPIO_0_PORT, &gpio_init);
    GPIO_PinAFConfig(BOARD_BOOT_APP_SEL_MODE_GPIO_0_PORT, gpio_init.Pins, GPIO_AF_15);

    /* led. */
    gpio_init.Pins  = BOARD_BOOT_APP_SEL_LED_GPIO_0_PIN;
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull; /* output. */
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(BOARD_BOOT_APP_SEL_LED_GPIO_0_PORT, &gpio_init);
    GPIO_PinAFConfig(BOARD_BOOT_APP_SEL_LED_GPIO_0_PORT, gpio_init.Pins, GPIO_AF_15);
    GPIO_WriteBit(BOARD_BOOT_APP_SEL_LED_GPIO_0_PORT, BOARD_BOOT_APP_SEL_LED_GPIO_0_PIN, 1u); /* led off. */

    if ( GPIO_ReadInputDataBit(BOARD_BOOT_APP_SEL_MODE_GPIO_0_PORT, BOARD_BOOT_APP_SEL_MODE_GPIO_0_PIN) )
    {
        boot_mode = BOARD_BootAppSelMode_Normal;
        GPIO_WriteBit(BOARD_BOOT_APP_SEL_LED_GPIO_0_PORT, BOARD_BOOT_APP_SEL_LED_GPIO_0_PIN, 1u); /* led off. */
    }
    else
    {
        boot_mode = BOARD_BootAppSelMode_Loader;
        GPIO_WriteBit(BOARD_BOOT_APP_SEL_LED_GPIO_0_PORT, BOARD_BOOT_APP_SEL_LED_GPIO_0_PIN, 0u); /* led on. */
    }

    return boot_mode;
}
#endif

/* EOF. */

