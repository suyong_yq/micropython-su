/* machine_pin_board_pins. */

#include "hal_common.h"
#include "machine_pin.h"
//#include "machine_dac.h"
#include "machine_adc.h"
#include "machine_uart.h"
//#include "machine_spi.h"
//#include "machine_i2c.h"
#include "machine_pwm.h"
#include "machine_timer.h"

extern const mp_obj_type_t machine_pin_type;

const uint32_t machine_pin_board_pins_num = 144;

const machine_pin_obj_t pin_PE2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE2, .gpio_port = GPIOE, .gpio_pin = 2 };
const machine_pin_obj_t pin_PE3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE3, .gpio_port = GPIOE, .gpio_pin = 3 };
const machine_pin_obj_t pin_PE4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE4, .gpio_port = GPIOE, .gpio_pin = 4 };
const machine_pin_obj_t pin_PE5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE5, .gpio_port = GPIOE, .gpio_pin = 5 };
const machine_pin_obj_t pin_PE6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE6, .gpio_port = GPIOE, .gpio_pin = 6 };
const machine_pin_obj_t pin_PC13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC13, .gpio_port = GPIOC, .gpio_pin = 13 };
const machine_pin_obj_t pin_PC14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC14, .gpio_port = GPIOC, .gpio_pin = 14 };
const machine_pin_obj_t pin_PC15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC15, .gpio_port = GPIOC, .gpio_pin = 15 };
const machine_pin_obj_t pin_PF0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF0, .gpio_port = GPIOF, .gpio_pin = 0 };
const machine_pin_obj_t pin_PF1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF1, .gpio_port = GPIOF, .gpio_pin = 1 };
const machine_pin_obj_t pin_PF2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF2, .gpio_port = GPIOF, .gpio_pin = 2 };
const machine_pin_obj_t pin_PF3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF3, .gpio_port = GPIOF, .gpio_pin = 3 };
const machine_pin_obj_t pin_PF4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF4, .gpio_port = GPIOF, .gpio_pin = 4 };
const machine_pin_obj_t pin_PF5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF5, .gpio_port = GPIOF, .gpio_pin = 5 };
const machine_pin_obj_t pin_PF6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF6, .gpio_port = GPIOF, .gpio_pin = 6 };
const machine_pin_obj_t pin_PF7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF7, .gpio_port = GPIOF, .gpio_pin = 7 };
const machine_pin_obj_t pin_PF8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF8, .gpio_port = GPIOF, .gpio_pin = 8 };
const machine_pin_obj_t pin_PF9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF9, .gpio_port = GPIOF, .gpio_pin = 9 };
const machine_pin_obj_t pin_PF10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF10, .gpio_port = GPIOF, .gpio_pin = 10 };
const machine_pin_obj_t pin_PH0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PH0, .gpio_port = GPIOH, .gpio_pin = 0 };
const machine_pin_obj_t pin_PH1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PH1, .gpio_port = GPIOH, .gpio_pin = 1 };
const machine_pin_obj_t pin_PC0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC0, .gpio_port = GPIOC, .gpio_pin = 0 };
const machine_pin_obj_t pin_PC1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC1, .gpio_port = GPIOC, .gpio_pin = 1 };
const machine_pin_obj_t pin_PC2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC2, .gpio_port = GPIOC, .gpio_pin = 2 };
const machine_pin_obj_t pin_PC3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC3, .gpio_port = GPIOC, .gpio_pin = 3 };
const machine_pin_obj_t pin_PA0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA0, .gpio_port = GPIOA, .gpio_pin = 0 };
const machine_pin_obj_t pin_PA1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA1, .gpio_port = GPIOA, .gpio_pin = 1 };
const machine_pin_obj_t pin_PA2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA2, .gpio_port = GPIOA, .gpio_pin = 2 };
const machine_pin_obj_t pin_PA3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA3, .gpio_port = GPIOA, .gpio_pin = 3 };
const machine_pin_obj_t pin_PA4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA4, .gpio_port = GPIOA, .gpio_pin = 4 };
const machine_pin_obj_t pin_PA5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA5, .gpio_port = GPIOA, .gpio_pin = 5 };
const machine_pin_obj_t pin_PA6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA6, .gpio_port = GPIOA, .gpio_pin = 6 };
const machine_pin_obj_t pin_PA7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA7, .gpio_port = GPIOA, .gpio_pin = 7 };
const machine_pin_obj_t pin_PC4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC4, .gpio_port = GPIOC, .gpio_pin = 4 };
const machine_pin_obj_t pin_PC5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC5, .gpio_port = GPIOC, .gpio_pin = 5 };
const machine_pin_obj_t pin_PB0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB0, .gpio_port = GPIOB, .gpio_pin = 0 };
const machine_pin_obj_t pin_PB1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB1, .gpio_port = GPIOB, .gpio_pin = 1 };
const machine_pin_obj_t pin_PB2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB2, .gpio_port = GPIOB, .gpio_pin = 2 };
const machine_pin_obj_t pin_PF11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF11, .gpio_port = GPIOF, .gpio_pin = 11 };
const machine_pin_obj_t pin_PF12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF12, .gpio_port = GPIOF, .gpio_pin = 12 };
const machine_pin_obj_t pin_PF13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF13, .gpio_port = GPIOF, .gpio_pin = 13 };
const machine_pin_obj_t pin_PF14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF14, .gpio_port = GPIOF, .gpio_pin = 14 };
const machine_pin_obj_t pin_PF15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PF15, .gpio_port = GPIOF, .gpio_pin = 15 };
const machine_pin_obj_t pin_PG0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG0, .gpio_port = GPIOG, .gpio_pin = 0 };
const machine_pin_obj_t pin_PG1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG1, .gpio_port = GPIOG, .gpio_pin = 1 };
const machine_pin_obj_t pin_PE7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE7, .gpio_port = GPIOE, .gpio_pin = 7 };
const machine_pin_obj_t pin_PE8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE8, .gpio_port = GPIOE, .gpio_pin = 8 };
const machine_pin_obj_t pin_PE9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE9, .gpio_port = GPIOE, .gpio_pin = 9 };
const machine_pin_obj_t pin_PE10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE10, .gpio_port = GPIOE, .gpio_pin = 10 };
const machine_pin_obj_t pin_PE11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE11, .gpio_port = GPIOE, .gpio_pin = 11 };
const machine_pin_obj_t pin_PE12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE12, .gpio_port = GPIOE, .gpio_pin = 12 };
const machine_pin_obj_t pin_PE13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE13, .gpio_port = GPIOE, .gpio_pin = 13 };
const machine_pin_obj_t pin_PE14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE14, .gpio_port = GPIOE, .gpio_pin = 14 };
const machine_pin_obj_t pin_PE15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE15, .gpio_port = GPIOE, .gpio_pin = 15 };
const machine_pin_obj_t pin_PB10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB10, .gpio_port = GPIOB, .gpio_pin = 10 };
const machine_pin_obj_t pin_PB11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB11, .gpio_port = GPIOB, .gpio_pin = 11 };
const machine_pin_obj_t pin_PB12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB12, .gpio_port = GPIOB, .gpio_pin = 12 };
const machine_pin_obj_t pin_PB13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB13, .gpio_port = GPIOB, .gpio_pin = 13 };
const machine_pin_obj_t pin_PB14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB14, .gpio_port = GPIOB, .gpio_pin = 14 };
const machine_pin_obj_t pin_PB15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB15, .gpio_port = GPIOB, .gpio_pin = 15 };
const machine_pin_obj_t pin_PD8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD8, .gpio_port = GPIOD, .gpio_pin = 8 };
const machine_pin_obj_t pin_PD9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD9, .gpio_port = GPIOD, .gpio_pin = 9 };
const machine_pin_obj_t pin_PD10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD10, .gpio_port = GPIOD, .gpio_pin = 10 };
const machine_pin_obj_t pin_PD11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD11, .gpio_port = GPIOD, .gpio_pin = 11 };
const machine_pin_obj_t pin_PD12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD12, .gpio_port = GPIOD, .gpio_pin = 12 };
const machine_pin_obj_t pin_PD13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD13, .gpio_port = GPIOD, .gpio_pin = 13 };
const machine_pin_obj_t pin_PD14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD14, .gpio_port = GPIOD, .gpio_pin = 14 };
const machine_pin_obj_t pin_PD15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD15, .gpio_port = GPIOD, .gpio_pin = 15 };
const machine_pin_obj_t pin_PG2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG2, .gpio_port = GPIOG, .gpio_pin = 2 };
const machine_pin_obj_t pin_PG3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG3, .gpio_port = GPIOG, .gpio_pin = 3 };
const machine_pin_obj_t pin_PG4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG4, .gpio_port = GPIOG, .gpio_pin = 4 };
const machine_pin_obj_t pin_PG5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG5, .gpio_port = GPIOG, .gpio_pin = 5 };
const machine_pin_obj_t pin_PG6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG6, .gpio_port = GPIOG, .gpio_pin = 6 };
const machine_pin_obj_t pin_PG7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG7, .gpio_port = GPIOG, .gpio_pin = 7 };
const machine_pin_obj_t pin_PG8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG8, .gpio_port = GPIOG, .gpio_pin = 8 };
const machine_pin_obj_t pin_PC6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC6, .gpio_port = GPIOC, .gpio_pin = 6 };
const machine_pin_obj_t pin_PC7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC7, .gpio_port = GPIOC, .gpio_pin = 7 };
const machine_pin_obj_t pin_PC8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC8, .gpio_port = GPIOC, .gpio_pin = 8 };
const machine_pin_obj_t pin_PC9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC9, .gpio_port = GPIOC, .gpio_pin = 9 };
const machine_pin_obj_t pin_PA8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA8, .gpio_port = GPIOA, .gpio_pin = 8 };
const machine_pin_obj_t pin_PA9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA9, .gpio_port = GPIOA, .gpio_pin = 9 };
const machine_pin_obj_t pin_PA10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA10, .gpio_port = GPIOA, .gpio_pin = 10 };
const machine_pin_obj_t pin_PA11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA11, .gpio_port = GPIOA, .gpio_pin = 11 };
const machine_pin_obj_t pin_PA12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA12, .gpio_port = GPIOA, .gpio_pin = 12 };
const machine_pin_obj_t pin_PA13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA13, .gpio_port = GPIOA, .gpio_pin = 13 };
const machine_pin_obj_t pin_PH2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PH2, .gpio_port = GPIOH, .gpio_pin = 2 };
const machine_pin_obj_t pin_PA14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA14, .gpio_port = GPIOA, .gpio_pin = 14 };
const machine_pin_obj_t pin_PA15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PA15, .gpio_port = GPIOA, .gpio_pin = 15 };
const machine_pin_obj_t pin_PC10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC10, .gpio_port = GPIOC, .gpio_pin = 10 };
const machine_pin_obj_t pin_PC11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC11, .gpio_port = GPIOC, .gpio_pin = 11 };
const machine_pin_obj_t pin_PC12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PC12, .gpio_port = GPIOC, .gpio_pin = 12 };
const machine_pin_obj_t pin_PD0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD0, .gpio_port = GPIOD, .gpio_pin = 0 };
const machine_pin_obj_t pin_PD1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD1, .gpio_port = GPIOD, .gpio_pin = 1 };
const machine_pin_obj_t pin_PD2 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD2, .gpio_port = GPIOD, .gpio_pin = 2 };
const machine_pin_obj_t pin_PD3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD3, .gpio_port = GPIOD, .gpio_pin = 3 };
const machine_pin_obj_t pin_PD4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD4, .gpio_port = GPIOD, .gpio_pin = 4 };
const machine_pin_obj_t pin_PD5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD5, .gpio_port = GPIOD, .gpio_pin = 5 };
const machine_pin_obj_t pin_PD6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD6, .gpio_port = GPIOD, .gpio_pin = 6 };
const machine_pin_obj_t pin_PD7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PD7, .gpio_port = GPIOD, .gpio_pin = 7 };
const machine_pin_obj_t pin_PG9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG9, .gpio_port = GPIOG, .gpio_pin = 9 };
const machine_pin_obj_t pin_PG10 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG10, .gpio_port = GPIOG, .gpio_pin = 10 };
const machine_pin_obj_t pin_PG11 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG11, .gpio_port = GPIOG, .gpio_pin = 11 };
const machine_pin_obj_t pin_PG12 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG12, .gpio_port = GPIOG, .gpio_pin = 12 };
const machine_pin_obj_t pin_PG13 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG13, .gpio_port = GPIOG, .gpio_pin = 13 };
const machine_pin_obj_t pin_PG14 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG14, .gpio_port = GPIOG, .gpio_pin = 14 };
const machine_pin_obj_t pin_PG15 = { .base = { &machine_pin_type }, .name = MP_QSTR_PG15, .gpio_port = GPIOG, .gpio_pin = 15 };
const machine_pin_obj_t pin_PB3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB3, .gpio_port = GPIOB, .gpio_pin = 3 };
const machine_pin_obj_t pin_PB4 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB4, .gpio_port = GPIOB, .gpio_pin = 4 };
const machine_pin_obj_t pin_PB5 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB5, .gpio_port = GPIOB, .gpio_pin = 5 };
const machine_pin_obj_t pin_PB6 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB6, .gpio_port = GPIOB, .gpio_pin = 6 };
const machine_pin_obj_t pin_PB7 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB7, .gpio_port = GPIOB, .gpio_pin = 7 };
const machine_pin_obj_t pin_PH3 = { .base = { &machine_pin_type }, .name = MP_QSTR_PH3, .gpio_port = GPIOH, .gpio_pin = 3 };
const machine_pin_obj_t pin_PB8 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB8, .gpio_port = GPIOB, .gpio_pin = 8 };
const machine_pin_obj_t pin_PB9 = { .base = { &machine_pin_type }, .name = MP_QSTR_PB9, .gpio_port = GPIOB, .gpio_pin = 9 };
const machine_pin_obj_t pin_PE0 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE0, .gpio_port = GPIOE, .gpio_pin = 0 };
const machine_pin_obj_t pin_PE1 = { .base = { &machine_pin_type }, .name = MP_QSTR_PE1, .gpio_port = GPIOE, .gpio_pin = 1 };

/* pin id in the package. */
const machine_pin_obj_t* machine_pin_board_pins[] =
{
    &pin_PE2,
    &pin_PE3,
    &pin_PE4,
    &pin_PE5,
    &pin_PE6,
    NULL, /* VBAT */
    &pin_PC13,
    &pin_PC14,
    &pin_PC15,
    &pin_PF0,
    &pin_PF1,
    &pin_PF2,
    &pin_PF3,
    &pin_PF4,
    &pin_PF5,
    NULL, /* VSS_5 */
    NULL, /* VDD_5 */
    &pin_PF6,
    &pin_PF7,
    &pin_PF8,
    &pin_PF9,
    &pin_PF10,
    &pin_PH0,
    &pin_PH1,
    NULL, /* NRST */
    &pin_PC0,
    &pin_PC1,
    &pin_PC2,
    &pin_PC3,
    NULL, /* VSSA */
    NULL, /* VREF_N */
    NULL, /* VREF_P */
    NULL, /* VDDA */
    &pin_PA0,
    &pin_PA1,
    &pin_PA2,
    &pin_PA3,
    NULL, /* VSS_4 */
    NULL, /* VDD_4 */
    &pin_PA4,
    &pin_PA5,
    &pin_PA6,
    &pin_PA7,
    &pin_PC4,
    &pin_PC5,
    &pin_PB0,
    &pin_PB1,
    &pin_PB2,
    &pin_PF11,
    &pin_PF12,
    NULL, /* VSS_6 */
    NULL, /* VDD_6 */
    &pin_PF13,
    &pin_PF14,
    &pin_PF15,
    &pin_PG0,
    &pin_PG1,
    &pin_PE7,
    &pin_PE8,
    &pin_PE9,
    NULL, /* VSS_7 */
    NULL, /* VDD_7 */
    &pin_PE10,
    &pin_PE11,
    &pin_PE12,
    &pin_PE13,
    &pin_PE14,
    &pin_PE15,
    &pin_PB10,
    &pin_PB11,
    NULL, /* VSS_1 */
    NULL, /* VDD_1 */
    &pin_PB12,
    &pin_PB13,
    &pin_PB14,
    &pin_PB15,
    &pin_PD8,
    &pin_PD9,
    &pin_PD10,
    &pin_PD11,
    &pin_PD12,
    &pin_PD13,
    NULL, /* VSS_8 */
    NULL, /* VDD_8 */
    &pin_PD14,
    &pin_PD15,
    &pin_PG2,
    &pin_PG3,
    &pin_PG4,
    &pin_PG5,
    &pin_PG6,
    &pin_PG7,
    &pin_PG8,
    NULL, /* VSS_9 */
    NULL, /* VDD_9 */
    &pin_PC6,
    &pin_PC7,
    &pin_PC8,
    &pin_PC9,
    &pin_PA8,
    &pin_PA9,
    &pin_PA10,
    &pin_PA11,
    &pin_PA12,
    &pin_PA13,
    &pin_PH2,
    NULL, /* VSS_2 */
    NULL, /* VDD_2 */
    &pin_PA14,
    &pin_PA15,
    &pin_PC10,
    &pin_PC11,
    &pin_PC12,
    &pin_PD0,
    &pin_PD1,
    &pin_PD2,
    &pin_PD3,
    &pin_PD4,
    &pin_PD5,
    NULL, /* VSS_10 */
    NULL, /* VDD_10 */
    &pin_PD6,
    &pin_PD7,
    &pin_PG9,
    &pin_PG10,
    &pin_PG11,
    &pin_PG12,
    &pin_PG13,
    &pin_PG14,
    NULL, /* VSS_11 */
    NULL, /* VDD_11 */
    &pin_PG15,
    &pin_PB3,
    &pin_PB4,
    &pin_PB5,
    &pin_PB6,
    &pin_PB7,
    &pin_PH3,
    &pin_PB8,
    &pin_PB9,
    &pin_PE0,
    &pin_PE1,
    NULL, /* VSS_3 */
    NULL, /* VDD_3 */
};

STATIC const mp_rom_map_elem_t machine_pin_board_pins_locals_dict_table[] =
{
    { MP_ROM_QSTR(MP_QSTR_PE2), MP_ROM_PTR(&pin_PE2) },
    { MP_ROM_QSTR(MP_QSTR_PE3), MP_ROM_PTR(&pin_PE3) },
    { MP_ROM_QSTR(MP_QSTR_PE4), MP_ROM_PTR(&pin_PE4) },
    { MP_ROM_QSTR(MP_QSTR_PE5), MP_ROM_PTR(&pin_PE5) },
    { MP_ROM_QSTR(MP_QSTR_PE6), MP_ROM_PTR(&pin_PE6) },
    { MP_ROM_QSTR(MP_QSTR_PC13), MP_ROM_PTR(&pin_PC13) },
    { MP_ROM_QSTR(MP_QSTR_PC14), MP_ROM_PTR(&pin_PC14) },
    { MP_ROM_QSTR(MP_QSTR_PC15), MP_ROM_PTR(&pin_PC15) },
    { MP_ROM_QSTR(MP_QSTR_PF0), MP_ROM_PTR(&pin_PF0) },
    { MP_ROM_QSTR(MP_QSTR_PF1), MP_ROM_PTR(&pin_PF1) },
    { MP_ROM_QSTR(MP_QSTR_PF2), MP_ROM_PTR(&pin_PF2) },
    { MP_ROM_QSTR(MP_QSTR_PF3), MP_ROM_PTR(&pin_PF3) },
    { MP_ROM_QSTR(MP_QSTR_PF4), MP_ROM_PTR(&pin_PF4) },
    { MP_ROM_QSTR(MP_QSTR_PF5), MP_ROM_PTR(&pin_PF5) },
    { MP_ROM_QSTR(MP_QSTR_PF6), MP_ROM_PTR(&pin_PF6) },
    { MP_ROM_QSTR(MP_QSTR_PF7), MP_ROM_PTR(&pin_PF7) },
    { MP_ROM_QSTR(MP_QSTR_PF8), MP_ROM_PTR(&pin_PF8) },
    { MP_ROM_QSTR(MP_QSTR_PF9), MP_ROM_PTR(&pin_PF9) },
    { MP_ROM_QSTR(MP_QSTR_PF10), MP_ROM_PTR(&pin_PF10) },
    { MP_ROM_QSTR(MP_QSTR_PH0), MP_ROM_PTR(&pin_PH0) },
    { MP_ROM_QSTR(MP_QSTR_PH1), MP_ROM_PTR(&pin_PH1) },
    { MP_ROM_QSTR(MP_QSTR_PC0), MP_ROM_PTR(&pin_PC0) },
    { MP_ROM_QSTR(MP_QSTR_PC1), MP_ROM_PTR(&pin_PC1) },
    { MP_ROM_QSTR(MP_QSTR_PC2), MP_ROM_PTR(&pin_PC2) },
    { MP_ROM_QSTR(MP_QSTR_PC3), MP_ROM_PTR(&pin_PC3) },
    { MP_ROM_QSTR(MP_QSTR_PA0), MP_ROM_PTR(&pin_PA0) },
    { MP_ROM_QSTR(MP_QSTR_PA1), MP_ROM_PTR(&pin_PA1) },
    { MP_ROM_QSTR(MP_QSTR_PA2), MP_ROM_PTR(&pin_PA2) },
    { MP_ROM_QSTR(MP_QSTR_PA3), MP_ROM_PTR(&pin_PA3) },
    { MP_ROM_QSTR(MP_QSTR_PA4), MP_ROM_PTR(&pin_PA4) },
    { MP_ROM_QSTR(MP_QSTR_PA5), MP_ROM_PTR(&pin_PA5) },
    { MP_ROM_QSTR(MP_QSTR_PA6), MP_ROM_PTR(&pin_PA6) },
    { MP_ROM_QSTR(MP_QSTR_PA7), MP_ROM_PTR(&pin_PA7) },
    { MP_ROM_QSTR(MP_QSTR_PC4), MP_ROM_PTR(&pin_PC4) },
    { MP_ROM_QSTR(MP_QSTR_PC5), MP_ROM_PTR(&pin_PC5) },
    { MP_ROM_QSTR(MP_QSTR_PB0), MP_ROM_PTR(&pin_PB0) },
    { MP_ROM_QSTR(MP_QSTR_PB1), MP_ROM_PTR(&pin_PB1) },
    { MP_ROM_QSTR(MP_QSTR_PB2), MP_ROM_PTR(&pin_PB2) },
    { MP_ROM_QSTR(MP_QSTR_PF11), MP_ROM_PTR(&pin_PF11) },
    { MP_ROM_QSTR(MP_QSTR_PF12), MP_ROM_PTR(&pin_PF12) },
    { MP_ROM_QSTR(MP_QSTR_PF13), MP_ROM_PTR(&pin_PF13) },
    { MP_ROM_QSTR(MP_QSTR_PF14), MP_ROM_PTR(&pin_PF14) },
    { MP_ROM_QSTR(MP_QSTR_PF15), MP_ROM_PTR(&pin_PF15) },
    { MP_ROM_QSTR(MP_QSTR_PG0), MP_ROM_PTR(&pin_PG0) },
    { MP_ROM_QSTR(MP_QSTR_PG1), MP_ROM_PTR(&pin_PG1) },
    { MP_ROM_QSTR(MP_QSTR_PE7), MP_ROM_PTR(&pin_PE7) },
    { MP_ROM_QSTR(MP_QSTR_PE8), MP_ROM_PTR(&pin_PE8) },
    { MP_ROM_QSTR(MP_QSTR_PE9), MP_ROM_PTR(&pin_PE9) },
    { MP_ROM_QSTR(MP_QSTR_PE10), MP_ROM_PTR(&pin_PE10) },
    { MP_ROM_QSTR(MP_QSTR_PE11), MP_ROM_PTR(&pin_PE11) },
    { MP_ROM_QSTR(MP_QSTR_PE12), MP_ROM_PTR(&pin_PE12) },
    { MP_ROM_QSTR(MP_QSTR_PE13), MP_ROM_PTR(&pin_PE13) },
    { MP_ROM_QSTR(MP_QSTR_PE14), MP_ROM_PTR(&pin_PE14) },
    { MP_ROM_QSTR(MP_QSTR_PE15), MP_ROM_PTR(&pin_PE15) },
    { MP_ROM_QSTR(MP_QSTR_PB10), MP_ROM_PTR(&pin_PB10) },
    { MP_ROM_QSTR(MP_QSTR_PB11), MP_ROM_PTR(&pin_PB11) },
    { MP_ROM_QSTR(MP_QSTR_PB12), MP_ROM_PTR(&pin_PB12) },
    { MP_ROM_QSTR(MP_QSTR_PB13), MP_ROM_PTR(&pin_PB13) },
    { MP_ROM_QSTR(MP_QSTR_PB14), MP_ROM_PTR(&pin_PB14) },
    { MP_ROM_QSTR(MP_QSTR_PB15), MP_ROM_PTR(&pin_PB15) },
    { MP_ROM_QSTR(MP_QSTR_PD8), MP_ROM_PTR(&pin_PD8) },
    { MP_ROM_QSTR(MP_QSTR_PD9), MP_ROM_PTR(&pin_PD9) },
    { MP_ROM_QSTR(MP_QSTR_PD10), MP_ROM_PTR(&pin_PD10) },
    { MP_ROM_QSTR(MP_QSTR_PD11), MP_ROM_PTR(&pin_PD11) },
    { MP_ROM_QSTR(MP_QSTR_PD12), MP_ROM_PTR(&pin_PD12) },
    { MP_ROM_QSTR(MP_QSTR_PD13), MP_ROM_PTR(&pin_PD13) },
    { MP_ROM_QSTR(MP_QSTR_PD14), MP_ROM_PTR(&pin_PD14) },
    { MP_ROM_QSTR(MP_QSTR_PD15), MP_ROM_PTR(&pin_PD15) },
    { MP_ROM_QSTR(MP_QSTR_PG2), MP_ROM_PTR(&pin_PG2) },
    { MP_ROM_QSTR(MP_QSTR_PG3), MP_ROM_PTR(&pin_PG3) },
    { MP_ROM_QSTR(MP_QSTR_PG4), MP_ROM_PTR(&pin_PG4) },
    { MP_ROM_QSTR(MP_QSTR_PG5), MP_ROM_PTR(&pin_PG5) },
    { MP_ROM_QSTR(MP_QSTR_PG6), MP_ROM_PTR(&pin_PG6) },
    { MP_ROM_QSTR(MP_QSTR_PG7), MP_ROM_PTR(&pin_PG7) },
    { MP_ROM_QSTR(MP_QSTR_PG8), MP_ROM_PTR(&pin_PG8) },
    { MP_ROM_QSTR(MP_QSTR_PC6), MP_ROM_PTR(&pin_PC6) },
    { MP_ROM_QSTR(MP_QSTR_PC7), MP_ROM_PTR(&pin_PC7) },
    { MP_ROM_QSTR(MP_QSTR_PC8), MP_ROM_PTR(&pin_PC8) },
    { MP_ROM_QSTR(MP_QSTR_PC9), MP_ROM_PTR(&pin_PC9) },
    { MP_ROM_QSTR(MP_QSTR_PA8), MP_ROM_PTR(&pin_PA8) },
    { MP_ROM_QSTR(MP_QSTR_PA9), MP_ROM_PTR(&pin_PA9) },
    { MP_ROM_QSTR(MP_QSTR_PA10), MP_ROM_PTR(&pin_PA10) },
    { MP_ROM_QSTR(MP_QSTR_PA11), MP_ROM_PTR(&pin_PA11) },
    { MP_ROM_QSTR(MP_QSTR_PA12), MP_ROM_PTR(&pin_PA12) },
    { MP_ROM_QSTR(MP_QSTR_PA13), MP_ROM_PTR(&pin_PA13) },
    { MP_ROM_QSTR(MP_QSTR_PH2), MP_ROM_PTR(&pin_PH2) },
    { MP_ROM_QSTR(MP_QSTR_PA14), MP_ROM_PTR(&pin_PA14) },
    { MP_ROM_QSTR(MP_QSTR_PA15), MP_ROM_PTR(&pin_PA15) },
    { MP_ROM_QSTR(MP_QSTR_PC10), MP_ROM_PTR(&pin_PC10) },
    { MP_ROM_QSTR(MP_QSTR_PC11), MP_ROM_PTR(&pin_PC11) },
    { MP_ROM_QSTR(MP_QSTR_PC12), MP_ROM_PTR(&pin_PC12) },
    { MP_ROM_QSTR(MP_QSTR_PD0), MP_ROM_PTR(&pin_PD0) },
    { MP_ROM_QSTR(MP_QSTR_PD1), MP_ROM_PTR(&pin_PD1) },
    { MP_ROM_QSTR(MP_QSTR_PD2), MP_ROM_PTR(&pin_PD2) },
    { MP_ROM_QSTR(MP_QSTR_PD3), MP_ROM_PTR(&pin_PD3) },
    { MP_ROM_QSTR(MP_QSTR_PD4), MP_ROM_PTR(&pin_PD4) },
    { MP_ROM_QSTR(MP_QSTR_PD5), MP_ROM_PTR(&pin_PD5) },
    { MP_ROM_QSTR(MP_QSTR_PD6), MP_ROM_PTR(&pin_PD6) },
    { MP_ROM_QSTR(MP_QSTR_PD7), MP_ROM_PTR(&pin_PD7) },
    { MP_ROM_QSTR(MP_QSTR_PG9), MP_ROM_PTR(&pin_PG9) },
    { MP_ROM_QSTR(MP_QSTR_PG10), MP_ROM_PTR(&pin_PG10) },
    { MP_ROM_QSTR(MP_QSTR_PG11), MP_ROM_PTR(&pin_PG11) },
    { MP_ROM_QSTR(MP_QSTR_PG12), MP_ROM_PTR(&pin_PG12) },
    { MP_ROM_QSTR(MP_QSTR_PG13), MP_ROM_PTR(&pin_PG13) },
    { MP_ROM_QSTR(MP_QSTR_PG14), MP_ROM_PTR(&pin_PG14) },
    { MP_ROM_QSTR(MP_QSTR_PG15), MP_ROM_PTR(&pin_PG15) },
    { MP_ROM_QSTR(MP_QSTR_PB3), MP_ROM_PTR(&pin_PB3) },
    { MP_ROM_QSTR(MP_QSTR_PB4), MP_ROM_PTR(&pin_PB4) },
    { MP_ROM_QSTR(MP_QSTR_PB5), MP_ROM_PTR(&pin_PB5) },
    { MP_ROM_QSTR(MP_QSTR_PB6), MP_ROM_PTR(&pin_PB6) },
    { MP_ROM_QSTR(MP_QSTR_PB7), MP_ROM_PTR(&pin_PB7) },
    { MP_ROM_QSTR(MP_QSTR_PH3), MP_ROM_PTR(&pin_PH3) },
    { MP_ROM_QSTR(MP_QSTR_PB8), MP_ROM_PTR(&pin_PB8) },
    { MP_ROM_QSTR(MP_QSTR_PB9), MP_ROM_PTR(&pin_PB9) },
    { MP_ROM_QSTR(MP_QSTR_PE0), MP_ROM_PTR(&pin_PE0) },
    { MP_ROM_QSTR(MP_QSTR_PE1), MP_ROM_PTR(&pin_PE1) },
};

MP_DEFINE_CONST_DICT(machine_pin_board_pins_locals_dict, machine_pin_board_pins_locals_dict_table);


/* for UART. */
const uint32_t machine_uart_num = 7u;

machine_uart_conf_t machine_uart_conf[7]; /* static mamory instead of malloc(). */
UART_Type * const machine_uart_port[7] = {UART1, UART2, UART3, UART4, UART5, UART6, UART7};

const machine_uart_obj_t uart_0 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PB7 , .rx_pin_af = GPIO_AF_7, .tx_pin_obj = &pin_PB6 , .tx_pin_af = GPIO_AF_7, .uart_port = machine_uart_port[0], .uart_irqn = UART1_IRQn, .uart_id = 0u, .conf = &machine_uart_conf[0]};
const machine_uart_obj_t uart_1 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PA3 , .rx_pin_af = GPIO_AF_7, .tx_pin_obj = &pin_PA2 , .tx_pin_af = GPIO_AF_7, .uart_port = machine_uart_port[1], .uart_irqn = UART2_IRQn, .uart_id = 1u, .conf = &machine_uart_conf[1]};
const machine_uart_obj_t uart_2 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PB11, .rx_pin_af = GPIO_AF_7, .tx_pin_obj = &pin_PB10, .tx_pin_af = GPIO_AF_7, .uart_port = machine_uart_port[2], .uart_irqn = UART3_IRQn, .uart_id = 2u, .conf = &machine_uart_conf[2]};
const machine_uart_obj_t uart_3 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PC11, .rx_pin_af = GPIO_AF_8, .tx_pin_obj = &pin_PC10, .tx_pin_af = GPIO_AF_8, .uart_port = machine_uart_port[3], .uart_irqn = UART4_IRQn, .uart_id = 3u, .conf = &machine_uart_conf[3]};
const machine_uart_obj_t uart_4 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PA5 , .rx_pin_af = GPIO_AF_8, .tx_pin_obj = &pin_PA4,  .tx_pin_af = GPIO_AF_8, .uart_port = machine_uart_port[4], .uart_irqn = UART5_IRQn, .uart_id = 4u, .conf = &machine_uart_conf[4]};
const machine_uart_obj_t uart_5 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PB1 , .rx_pin_af = GPIO_AF_8, .tx_pin_obj = &pin_PB0 , .tx_pin_af = GPIO_AF_8, .uart_port = machine_uart_port[5], .uart_irqn = UART6_IRQn, .uart_id = 5u, .conf = &machine_uart_conf[5]};
const machine_uart_obj_t uart_6 = { .base = { &machine_uart_type }, .rx_pin_obj = &pin_PE7 , .rx_pin_af = GPIO_AF_8, .tx_pin_obj = &pin_PE8 , .tx_pin_af = GPIO_AF_8, .uart_port = machine_uart_port[6], .uart_irqn = UART7_IRQn, .uart_id = 6u, .conf = &machine_uart_conf[6]};

const machine_uart_obj_t * machine_uart_objs[] =
{
    &uart_0,
    &uart_1,
    &uart_2,
    &uart_3,
    &uart_4,
    &uart_5,
    &uart_6,
};

/* for PWM. */
machine_pwm_port_conf_t machine_pwm_port_conf[MACHINE_PWM_PORT_NUM];
machine_pwm_ch_conf_t machine_pwm_ch_conf[MACHIEN_PWM_CH_NUM_ALL];

/* tim2. */
const machine_pwm_obj_t pwm_0   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA0 ,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM2, .port_conf = &machine_pwm_port_conf[0], .tim_ch = 0, .ch_conf = &machine_pwm_ch_conf[0], .pwm_id = 0 };
const machine_pwm_obj_t pwm_1   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA1 ,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM2, .port_conf = &machine_pwm_port_conf[0], .tim_ch = 1, .ch_conf = &machine_pwm_ch_conf[1], .pwm_id = 1 };
const machine_pwm_obj_t pwm_2   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA2 ,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM2, .port_conf = &machine_pwm_port_conf[0], .tim_ch = 2, .ch_conf = &machine_pwm_ch_conf[2], .pwm_id = 2 };
const machine_pwm_obj_t pwm_3   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA3 ,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM2, .port_conf = &machine_pwm_port_conf[0], .tim_ch = 3, .ch_conf = &machine_pwm_ch_conf[3], .pwm_id = 3 };
/* tim3. */
const machine_pwm_obj_t pwm_4   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PB4 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM3, .port_conf = &machine_pwm_port_conf[1], .tim_ch = 0, .ch_conf = &machine_pwm_ch_conf[0], .pwm_id = 4 };
const machine_pwm_obj_t pwm_5   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PB5 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM3, .port_conf = &machine_pwm_port_conf[1], .tim_ch = 1, .ch_conf = &machine_pwm_ch_conf[1], .pwm_id = 5 };
const machine_pwm_obj_t pwm_6   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PB0 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM3, .port_conf = &machine_pwm_port_conf[1], .tim_ch = 2, .ch_conf = &machine_pwm_ch_conf[2], .pwm_id = 6 };
const machine_pwm_obj_t pwm_7   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PB1 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM3, .port_conf = &machine_pwm_port_conf[1], .tim_ch = 3, .ch_conf = &machine_pwm_ch_conf[3], .pwm_id = 7 };
/* tim4. */
const machine_pwm_obj_t pwm_8   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PD12,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM4, .port_conf = &machine_pwm_port_conf[2], .tim_ch = 0, .ch_conf = &machine_pwm_ch_conf[0], .pwm_id = 8 };
const machine_pwm_obj_t pwm_9   = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PD13,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM4, .port_conf = &machine_pwm_port_conf[2], .tim_ch = 1, .ch_conf = &machine_pwm_ch_conf[1], .pwm_id = 9 };
const machine_pwm_obj_t pwm_10  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PD14,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM4, .port_conf = &machine_pwm_port_conf[2], .tim_ch = 2, .ch_conf = &machine_pwm_ch_conf[2], .pwm_id = 10};
const machine_pwm_obj_t pwm_11  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PD15,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM4, .port_conf = &machine_pwm_port_conf[2], .tim_ch = 3, .ch_conf = &machine_pwm_ch_conf[3], .pwm_id = 11};
/* tim5. */
const machine_pwm_obj_t pwm_12  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PF6 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM5, .port_conf = &machine_pwm_port_conf[3], .tim_ch = 0, .ch_conf = &machine_pwm_ch_conf[0], .pwm_id = 12};
const machine_pwm_obj_t pwm_13  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PF7 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM5, .port_conf = &machine_pwm_port_conf[3], .tim_ch = 1, .ch_conf = &machine_pwm_ch_conf[1], .pwm_id = 13};
const machine_pwm_obj_t pwm_14  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PF8 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM5, .port_conf = &machine_pwm_port_conf[3], .tim_ch = 2, .ch_conf = &machine_pwm_ch_conf[2], .pwm_id = 14};
const machine_pwm_obj_t pwm_15  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PF9 ,  .pwm_pin_af = GPIO_AF_2, .tim_port = (TIM_Type *)TIM5, .port_conf = &machine_pwm_port_conf[3], .tim_ch = 3, .ch_conf = &machine_pwm_ch_conf[3], .pwm_id = 15};
/* tim1. */
const machine_pwm_obj_t pwm_16  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA8 ,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM1, .port_conf = &machine_pwm_port_conf[4], .tim_ch = 0, .ch_conf = &machine_pwm_ch_conf[0], .pwm_id = 16};
const machine_pwm_obj_t pwm_17  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA9 ,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM1, .port_conf = &machine_pwm_port_conf[4], .tim_ch = 1, .ch_conf = &machine_pwm_ch_conf[1], .pwm_id = 17};
const machine_pwm_obj_t pwm_18  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA10,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM1, .port_conf = &machine_pwm_port_conf[4], .tim_ch = 2, .ch_conf = &machine_pwm_ch_conf[2], .pwm_id = 18};
const machine_pwm_obj_t pwm_19  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PA11,  .pwm_pin_af = GPIO_AF_1, .tim_port = (TIM_Type *)TIM1, .port_conf = &machine_pwm_port_conf[4], .tim_ch = 3, .ch_conf = &machine_pwm_ch_conf[3], .pwm_id = 19};
/* tim8. */
const machine_pwm_obj_t pwm_20  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PC6 ,  .pwm_pin_af = GPIO_AF_3, .tim_port = (TIM_Type *)TIM8, .port_conf = &machine_pwm_port_conf[5], .tim_ch = 0, .ch_conf = &machine_pwm_ch_conf[0], .pwm_id = 20};
const machine_pwm_obj_t pwm_21  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PC7 ,  .pwm_pin_af = GPIO_AF_3, .tim_port = (TIM_Type *)TIM8, .port_conf = &machine_pwm_port_conf[5], .tim_ch = 1, .ch_conf = &machine_pwm_ch_conf[1], .pwm_id = 21};
const machine_pwm_obj_t pwm_22  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PC8 ,  .pwm_pin_af = GPIO_AF_3, .tim_port = (TIM_Type *)TIM8, .port_conf = &machine_pwm_port_conf[5], .tim_ch = 2, .ch_conf = &machine_pwm_ch_conf[2], .pwm_id = 22};
const machine_pwm_obj_t pwm_23  = {.base = { &machine_pwm_type }, .pwm_pin_obj = &pin_PC9 ,  .pwm_pin_af = GPIO_AF_3, .tim_port = (TIM_Type *)TIM8, .port_conf = &machine_pwm_port_conf[5], .tim_ch = 3, .ch_conf = &machine_pwm_ch_conf[3], .pwm_id = 23};


const machine_pwm_obj_t * machine_pwm_objs[] =
{
    &pwm_0,
    &pwm_1,
    &pwm_2,
    &pwm_3,
    &pwm_4,
    &pwm_5,
    &pwm_6,
    &pwm_7,
    &pwm_8,
    &pwm_9,
    &pwm_10,
    &pwm_11,
    &pwm_12,
    &pwm_13,
    &pwm_14,
    &pwm_15,
    &pwm_16,
    &pwm_17,
    &pwm_18,
    &pwm_19,
    &pwm_20,
    &pwm_21,
    &pwm_22,
    &pwm_23,
};



#if 0

/* for DAC pins. */
const uint32_t machine_dac_num = 2u;
machine_dac_conf_t machine_dac_conf[2]; /* static mamory instead of malloc(). */

const machine_dac_obj_t dac_CH0  = { .base = { &machine_dac_type }, .pin_obj = &pin_PA4, .dac_port = DAC, .dac_channel = 0u, .conf = &machine_dac_conf[0]  };
const machine_dac_obj_t dac_CH1  = { .base = { &machine_dac_type }, .pin_obj = &pin_PA5, .dac_port = DAC, .dac_channel = 1u, .conf = &machine_dac_conf[1]  };

const machine_dac_obj_t * machine_dac_objs[] =
{
    &dac_CH0 ,
    &dac_CH1 ,
};

#endif

/* for ADC pins. */
const machine_pin_obj_t pin_NUL = { .base = { &machine_pin_type } };
const uint32_t machine_adc_port_num = 2u;
const uint32_t machine_adc_channel_num_per_port = 16u;
const uint32_t machine_adc_channel_num = machine_adc_port_num * machine_adc_channel_num_per_port;

machine_adc_conf_t machine_adc_conf[3];
ADC_Type * const machine_adc_port[3] = {ADC1, ADC2};

const machine_adc_obj_t adc_CH0  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA0, .adc_port = machine_adc_port[0], .adc_channel = 0 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH1  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA1, .adc_port = machine_adc_port[0], .adc_channel = 1 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH2  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA2, .adc_port = machine_adc_port[0], .adc_channel = 2 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH3  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA3, .adc_port = machine_adc_port[0], .adc_channel = 3 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH4  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA4, .adc_port = machine_adc_port[0], .adc_channel = 4 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH5  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA5, .adc_port = machine_adc_port[0], .adc_channel = 5 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH6  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA6, .adc_port = machine_adc_port[0], .adc_channel = 6 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH7  = { .base = { &machine_adc_type }, .pin_obj = &pin_PA7, .adc_port = machine_adc_port[0], .adc_channel = 7 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH8  = { .base = { &machine_adc_type }, .pin_obj = &pin_PB0, .adc_port = machine_adc_port[0], .adc_channel = 8 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH9  = { .base = { &machine_adc_type }, .pin_obj = &pin_PB1, .adc_port = machine_adc_port[0], .adc_channel = 9 , .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH10 = { .base = { &machine_adc_type }, .pin_obj = &pin_PC0, .adc_port = machine_adc_port[0], .adc_channel = 10, .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH11 = { .base = { &machine_adc_type }, .pin_obj = &pin_PC1, .adc_port = machine_adc_port[0], .adc_channel = 11, .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH12 = { .base = { &machine_adc_type }, .pin_obj = &pin_PC2, .adc_port = machine_adc_port[0], .adc_channel = 12, .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH13 = { .base = { &machine_adc_type }, .pin_obj = &pin_PC3, .adc_port = machine_adc_port[0], .adc_channel = 13, .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH14 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[0], .adc_channel = 14, .conf = &machine_adc_conf[0] };
const machine_adc_obj_t adc_CH15 = { .base = { &machine_adc_type }, .pin_obj = &pin_PF9, .adc_port = machine_adc_port[0], .adc_channel = 15, .conf = &machine_adc_conf[0] };

const machine_adc_obj_t adc_CH16 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 0 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH17 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 1 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH18 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 2 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH19 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 3 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH20 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 4 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH21 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 5 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH22 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 6 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH23 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 7 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH24 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 8 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH25 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 9 , .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH26 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 10, .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH27 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 11, .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH28 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 12, .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH29 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 13, .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH30 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 14, .conf = &machine_adc_conf[1] };
const machine_adc_obj_t adc_CH31 = { .base = { &machine_adc_type }, .pin_obj = &pin_NUL, .adc_port = machine_adc_port[1], .adc_channel = 15, .conf = &machine_adc_conf[1] };

const machine_adc_obj_t * machine_adc_objs[] =
{
    &adc_CH0 ,
    &adc_CH1 ,
    &adc_CH2 ,
    &adc_CH3 ,
    &adc_CH4 ,
    &adc_CH5 ,
    &adc_CH6 ,
    &adc_CH7 ,
    &adc_CH8 ,
    &adc_CH9 ,
    &adc_CH10,
    &adc_CH11,
    &adc_CH12,
    &adc_CH13,
    &adc_CH14,
    &adc_CH15,
    &adc_CH16,
    &adc_CH17,
    &adc_CH18,
    &adc_CH19,
    &adc_CH20,
    &adc_CH20,
    &adc_CH22,
    &adc_CH23,
    &adc_CH24,
    &adc_CH25,
    &adc_CH26,
    &adc_CH27,
    &adc_CH28,
    &adc_CH29,
    &adc_CH30,
    &adc_CH31
};

/* CH14 & CH15 are for internal sensors. */


#if 0


/* for hardware SPI. */
STATIC machine_hw_spi_conf_t machine_hw_spi_conf[MACHINE_HW_SPI_NUM]; /* static mamory instead of malloc(). */

const machine_hw_spi_obj_t hw_spi_0 = {.base = { &machine_hw_spi_type }, .sck_pin_obj = &pin_PA5, .sck_pin_af = GPIO_AF_5, .mosi_pin_obj = &pin_PA7, .mosi_pin_af = GPIO_AF_5, .miso_pin_obj = &pin_PA6, .miso_pin_af = GPIO_AF_5, .spi_port = SPI1, .spi_irqn = SPI1_IRQn, .spi_id = 0u, .conf = &machine_hw_spi_conf[0]};
const machine_hw_spi_obj_t hw_spi_1 = {.base = { &machine_hw_spi_type }, .sck_pin_obj = &pin_PB10, .sck_pin_af = GPIO_AF_5, .mosi_pin_obj = &pin_PB15, .mosi_pin_af = GPIO_AF_5, .miso_pin_obj = &pin_PB14, .miso_pin_af = GPIO_AF_5, .spi_port = SPI2, .spi_irqn = SPI2_IRQn, .spi_id = 1u, .conf = &machine_hw_spi_conf[1]};
const machine_hw_spi_obj_t hw_spi_2 = {.base = { &machine_hw_spi_type }, .sck_pin_obj = &pin_PC9, .sck_pin_af = GPIO_AF_5, .mosi_pin_obj = &pin_PA8, .mosi_pin_af = GPIO_AF_5, .miso_pin_obj = &pin_PB9, .miso_pin_af = GPIO_AF_5, .spi_port = SPI3, .spi_irqn = SPI3_IRQn, .spi_id = 2u, .conf = &machine_hw_spi_conf[2]};

const machine_hw_spi_obj_t * machine_hw_spi_objs[] =
{
    &hw_spi_0,
    &hw_spi_1,
    &hw_spi_2,
};

/* for hardware I2C. */
STATIC machine_hw_i2c_conf_t machine_hw_i2c_conf[MACHINE_HW_I2C_NUM];

const machine_hw_i2c_obj_t hw_i2c_0 = {.base = { &machine_hw_i2c_type }, .scl_pin_obj = &pin_PC6, .scl_pin_af = GPIO_AF_4, .sda_pin_obj = &pin_PC7, .sda_pin_af = GPIO_AF_4, .i2c_port = I2C1, .i2c_id = 0u, .conf = &machine_hw_i2c_conf[0]};
const machine_hw_i2c_obj_t hw_i2c_1 = {.base = { &machine_hw_i2c_type }, .scl_pin_obj = &pin_PC2, .scl_pin_af = GPIO_AF_4, .sda_pin_obj = &pin_PC3, .sda_pin_af = GPIO_AF_4, .i2c_port = I2C2, .i2c_id = 1u, .conf = &machine_hw_i2c_conf[1]};

const machine_hw_i2c_obj_t * machine_hw_i2c_objs[] =
{
    &hw_i2c_0,
    &hw_i2c_1,
};


#endif

/* for timer. */
machine_timer_conf_t timer_conf[MACHINE_TIMER_NUM];

const machine_timer_obj_t timer0 = {.base = {&machine_timer_type}, .timer_port = (TIM_Type *)TIM6, .timer_irqn = TIM6_IRQn, .timer_id = 0u, .conf = &timer_conf[0]};
const machine_timer_obj_t timer1 = {.base = {&machine_timer_type}, .timer_port = (TIM_Type *)TIM7, .timer_irqn = TIM7_IRQn, .timer_id = 1u, .conf = &timer_conf[1]};

const machine_timer_obj_t * machine_timer_objs[] =
{
    &timer0 ,
    &timer1 ,
};



/* EOF. */

