/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Damien P. George
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "py/compile.h"
#include "py/runtime.h"
#include "py/gc.h"
#include "py/mperrno.h"
#include "py/stackctrl.h"
#include "lib/mp-readline/readline.h"
#include "lib/utils/gchelper.h"
#include "lib/utils/pyexec.h"

#include "machine_sdcard.h"

#include "board_init.h"

extern uint8_t _sstack, _estack, _gc_heap_start, _gc_heap_end;

bool mp_sdcard_is_ready = false;

void mp_hal_stdio_init(void);

int main(void)
{
    BOARD_Init();

    mp_hal_stdio_init();

    /* init sdcard hardware. */
    //mp_sdcard_is_ready = machine_sdcard_init();

    mp_stack_set_top(&_estack);
    mp_stack_set_limit(&_estack - &_sstack - 1024);

    for (;;)
    {
        gc_init(&_gc_heap_start, &_gc_heap_end);
        mp_init();

        mp_obj_list_init(MP_OBJ_TO_PTR(mp_sys_path), 0);
        mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR_));
        mp_obj_list_init(MP_OBJ_TO_PTR(mp_sys_argv), 0);

#if 0
        /* prepare the sdcard vfs and load py file from sdcard. */
        if (mp_sdcard_is_ready)
        {
            //printf("\r\n[Y] sdcard ready.\r\n");

            // create vfs object
            fs_user_mount_t *vfs_fat = m_new_obj_maybe(fs_user_mount_t);
            mp_vfs_mount_t  *vfs     = m_new_obj_maybe(mp_vfs_mount_t);
            if ( (vfs == NULL) || (vfs_fat == NULL) )
            {
                //printf("create vfs_fat & vfs failed.\r\n");
                break;
            }

            vfs_fat->blockdev.flags = MP_BLOCKDEV_FLAG_FREE_OBJ;
            machine_sdcard_init_vfs(vfs_fat);

            // try to mount the partition
            FRESULT res = f_mount(&vfs_fat->fatfs);

            if (res != FR_OK)
            {
                //printf("f_mount(&vfs_fat->fatfs) failed.\r\n");
                // couldn't mount
                m_del_obj(fs_user_mount_t, vfs_fat);
                m_del_obj(mp_vfs_mount_t , vfs);
            }
            else
            {
                vfs->str = "/sd";
                vfs->len = 3;
                vfs->obj = MP_OBJ_FROM_PTR(vfs_fat);
                vfs->next = NULL;
                for (mp_vfs_mount_t **m = &MP_STATE_VM(vfs_mount_table);; m = &(*m)->next) {
                    if (*m == NULL) {
                        *m = vfs;
                        break;
                    }
                }
                MP_STATE_PORT(vfs_cur) = vfs;

                //printf("[Y] file system on sdcard ready.\r\n");

                /* run the main.py in sdcard. */
                //mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR__slash_sd));
                //mp_obj_list_append(mp_sys_path, MP_OBJ_NEW_QSTR(MP_QSTR__slash_sd_slash_lib));
                const char * main_py = "boot.py";
                //printf("[Y] run the %s on disk ...\r\n", main_py);
                //int ret = pyexec_file_if_exists(main_py);
                pyexec_file_if_exists(main_py);
                //printf("[Y] done. %d\r\n", ret);
            }
        }
#endif

        // Initialize sub-systems.
        readline_init0();

        // Execute _boot.py to set up the filesystem.
        pyexec_frozen_module("_boot.py");

        pyexec_file_if_exists("boot.py");
        //pyexec_file_if_exists("main.py");


        /* run the repl. */
        //BOARD_InitDebugConsole();
        for (;;) {
            if (pyexec_mode_kind == PYEXEC_MODE_RAW_REPL) {
                if (pyexec_raw_repl() != 0) {
                    break;
                }
            } else {
                if (pyexec_friendly_repl() != 0) {
                    break;
                }
            }
        }

        mp_printf(MP_PYTHON_PRINTER, "MPY: soft reboot\n");
        gc_sweep_all();
        mp_deinit();
    }
}

void gc_collect(void) {
    gc_collect_start();
    gc_helper_collect_regs_and_stack();
    gc_collect_end();
}

#if 0
mp_lexer_t *mp_lexer_new_from_file(const char *filename) {
    mp_raise_OSError(MP_ENOENT);
}

mp_import_stat_t mp_import_stat(const char *path) {
    return MP_IMPORT_STAT_NO_EXIST;
}

mp_obj_t mp_builtin_open(size_t n_args, const mp_obj_t *args, mp_map_t *kwargs) {
    return mp_const_none;
}
MP_DEFINE_CONST_FUN_OBJ_KW(mp_builtin_open_obj, 1, mp_builtin_open);
#endif

void nlr_jump_fail(void *val) {
    for (;;) {
    }
}

#ifndef NDEBUG
void MP_WEAK __assert_func(const char *file, int line, const char *func, const char *expr) {
    mp_printf(MP_PYTHON_PRINTER, "Assertion '%s' failed, at file %s:%d\n", expr, file, line);
    for (;;) {
    }
}
#endif

void _start(void)
{
    main();

    for (;;)
    {
    }
}

/* EOF. */

