from machine import Timer, Pin

led0 = Pin('PC9', mode=Pin.OUT_PUSHPULL, value=1)

def t0_callback(self):
    led0(1-led0())

t0 = Timer(0, mode=Timer.PERIODIC, callback=t0_callback, period=100)

while True:
    pass

