from machine import Pin
from machine import Timer
import utime

led0 = Pin('PA15', mode=Pin.OUT_PUSHPULL)

def t0_callback(self):
    led0(1-led0())

t0 = Timer(0, mode=Timer.PERIODIC, callback=t0_callback, period=100)

while True:
    pass


