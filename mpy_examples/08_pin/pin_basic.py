from machine import Pin
import utime

led0 = Pin('PA2', mode=Pin.OUT_PUSHPULL, value=1)
led1 = Pin('PA3', mode=Pin.OUT_PUSHPULL, value=0)

for i in range(20):
    utime.sleep_ms(200)
    led0(1-led0())
    led1(1-led1())


