from machine import SPI
from machine import Pin

spiflash_spi=SPI(1, baudrate=10000, polarity=0, phase=1, firstbit=SPI.MSB)
spiflash_cs =Pin('PE3', mode=Pin.OUT_PUSHPULL, value=1)

spiflash_cs.low()
spiflash_spi.write(chr(int('9f',16)))
spiid = spiflash_spi.read(3)
spiflash_cs.high()



'''
mosi=Pin('PB15', mode=Pin.OUT_PUSHPULL)
miso=Pin('PB14', mode=Pin.OUT_PUSHPULL)
sck =Pin('PB10', mode=Pin.OUT_PUSHPULL)
cs  =Pin('PE3' , mode=Pin.OUT_PUSHPULL)

spipins=[mosi, miso, sck, cs]

for i in range(20):
    for spipin in spipins:
        spipin.high()
        spipin.low()

'''


