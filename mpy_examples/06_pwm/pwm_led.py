from machine import Pin
from machine import PWM
import time

pwm0 = PWM(Pin('PC9'), freq=10000, duty=0)
d=0
while True:
    pwm0.duty(d)
    d=(d+1)%1000
    time.sleep_ms(2)

